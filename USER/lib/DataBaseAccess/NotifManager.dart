import 'dart:async';
import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:virusaround/MainScreens/LogsView.dart';


class NotifManager {
  static const String SERVER_TOKEN =
      'AAAAh-rTwhM:APA91bG_lmvJM2g9Ie4TDhdkhM5UUYY4WALi1Q_6PMAhXf6c6JqiGuKP9457gwQWQF7iiKaJp_zIDP1Ab31s4KQ1q0JUy2_L6cR_XQvBcutHrrv1NNa-nph8IWumwz0Hm53xe_IL3cec';

  static final FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  static final Completer<Map<String, dynamic>> completer =
      Completer<Map<String, dynamic>>();

  static init() {
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: false),
    );
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        Logs.addNode("NotifManager", "onMessage", message.toString());
        Logs.needToShowDialog = true;
//        Logs.orderId = message['order']
        completer.complete(message);
      },
//      onBackgroundMessage: (Map<String, dynamic> message) async {
//        print("NotifManager: " + "onLaunch: $message");
//        Logs.log += "\nonMessage\n" + message.toString() + "\n\n";
//        completer.complete(message);
//        //_navigateToItemDetail(message);
//      },
      onLaunch: (Map<String, dynamic> message) async {
        Logs.addNode("NotifManager", "onLaunch", message.toString());
        completer.complete(message);
        //_navigateToItemDetail(message);
      },
      onResume: (Map<String, dynamic> message) async {
        Logs.addNode("NotifManager", "onResume", message.toString());
        completer.complete(message);
        //_navigateToItemDetail(message);
      },
    );
  }

  static GetDeviceToken() async {
    return await firebaseMessaging.getToken();
  }

  static GetAcceptWordByGender(String gender){
    if(gender == "Муж")
      return "Подтвердил";
    return "Потдтвердила";
  }

  static NotifyWorkersOfConfirmation(
      List devicesTokens, String orderId, String name, String orderName) async {
    devicesTokens.forEach((token) async {
      print(token);
      //await Future.delayed(const Duration(seconds: 2), () {});
      await http.post(
        'https://fcm.googleapis.com/fcm/send',
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'key=$SERVER_TOKEN',
        },
        body: jsonEncode(
          <String, dynamic>{
            'notification': <String, dynamic>{
              'body': "Вас одобрили для заказа!",
              'title': "Пойдете на работу $orderName, $name?"
            },
            'priority': 'high',
            'data': <String, dynamic>{
              'click_action': 'FLUTTER_NOTIFICATION_CLICK',
              'id': '1',
              'status': 'done',
              'class': 'acceptToApplied',
              'orderId': orderId,
            },
            'to': token,
          },
        ),
      );
    });
  }

  static Future<Map<String, dynamic>> sendAndRetrieveMessage() async {
    await firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(
          sound: true, badge: true, alert: true, provisional: false),
    );

    await http.post(
      'https://fcm.googleapis.com/fcm/send',
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'key=$SERVER_TOKEN',
      },
      body: jsonEncode(
        <String, dynamic>{
          'notification': <String, dynamic>{
            'body': 'this is a body',
            'title': 'this is a title'
          },
          'priority': 'high',
          'data': <String, dynamic>{
            'click_action': 'FLUTTER_NOTIFICATION_CLICK',
            'id': '1',
            'status': 'done'
          },
          'to':
              'eUXgNCBthAE:APA91bFMF26PoKkBRevu6GhhyHOUWLW1u77z6yGp72FccselrKwqzoEdmWsz7bcIG6ZWliG8a1k41owm0FCrzCQOm9MI7mtU8v_p4mw7e1CtQbSsmZ47xhS9xt5lxSEz4Ut7s_LC5vkd',
        },
      ),
    );

    final Completer<Map<String, dynamic>> completer =
        Completer<Map<String, dynamic>>();

    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        completer.complete(message);
      },
    );

    return completer.future;
  }
}

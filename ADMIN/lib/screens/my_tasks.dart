import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:virusaround_admin/service/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'home_screen.dart';

class MyTaskPage extends StatefulWidget {
  @override
  _MyTaskPageState createState() => _MyTaskPageState();
}

class _MyTaskPageState extends State<MyTaskPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[

          FlatButton(
            child: Text('Доска заказов',style: TextStyle(color: Colors.white)),
            onPressed: (){
              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>HomePage()));
            },

          ),
          FlatButton(
            child: Text('Мои заказы',style: TextStyle(color: Colors.red)),
          ),

          FlatButton(
            child: Text(UserInfo.Email.toString(), style: TextStyle(color: Colors.white)),
          ),
          SizedBox(width: 20,),
          FlatButton(
            onPressed: () {
              AuthService().signOut();
            },
            child: Center(
              child: Text('ВЫЙТИ' ,style: TextStyle(color: Colors.red),),
            ),
          ),

        ],
        title: RichText(
          text: new TextSpan(
            children: [
              new TextSpan(
                text: 'VIRUS',
                style: new TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              new TextSpan(
                text: 'AROUND ',
                style: new TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              new TextSpan(
                text: 'Админ панель',
                style: new TextStyle(color: Colors.white),
              ),


            ],
          ),
        ),

      ),
      body: Center(
        child: Container(
            padding: const EdgeInsets.all(10.0),
            child: StreamBuilder<QuerySnapshot>(
              stream: Firestore.instance.collection('orders').where('company', arrayContains: UserInfo.Uid).snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.hasError)
                  return new Text('Error: ${snapshot.error}');
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return new CircularProgressIndicator();
                  default:
                    return new ListView(
                      children: snapshot.data.documents
                          .map((DocumentSnapshot document) {
                        return new CustomCard(
                     document: document,
                        );
                      }).toList(),
                    );
                }
              },
            )),
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  CustomCard({@required this.document});

  final DocumentSnapshot document;


  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
            onTap: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return
                      AlertDialog(
                        title: Text(document['title'],
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16.0)),
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(document['body'],
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 16.0)),
                            SizedBox(height: 50),
                            Text(document['address'],
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 16.0)),
                            SizedBox(height: 50),
                            Text('Контакты: ${document['contacts']}',
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 16.0)),
                            SizedBox(height: 50),
                            InkWell(
                              onTap: () {
                                launch(
                                    'https://www.google.com/maps/search/?api=1&query=${document['geopos']['geopoint'].latitude.toString()},${document['geopos']['geopoint'].longitude.toString()}');
                              },
                              child: Image.network(
                                'https://static-maps.yandex.ru/1.x/?l=map&pt=${document['geopos']['geopoint'].longitude.toString()},${document['geopos']['geopoint'].latitude.toString()},pm2dgl,l&z=15&&size=600,300',
                                width: 320,
                                fit: BoxFit.cover,
                              ),
                            ),
                            SizedBox(height: 50),

                            Text(document['body'],
                                style: TextStyle(
                                    fontWeight: FontWeight.normal,
                                    fontSize: 16.0)),
                            SizedBox(height: 50),
                          ],
                        ),
                      );
                  });
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 2.0),
                  child: Text(
                    document['title'],
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 2.0),
                  child: Text(
                    document['body'],
                    style: TextStyle(
                        fontWeight: FontWeight.normal, fontSize: 16.0),
                  ),
                ),
                SizedBox(height: 15),
              ],
            )));
  }
}

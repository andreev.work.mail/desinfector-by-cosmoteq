import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../DataBaseAccess/FBManager.dart';
import '../main.dart';
import '../Settings/userSettings.dart';

class StepperDemo extends StatefulWidget {
  StepperDemo() : super();

  final String title = "Stepper Demo";

  @override
  StepperDemoState createState() => StepperDemoState();
}

class StepperDemoState extends State<StepperDemo> {
  int current_step = 0;

  static String _kind;
  String _name;
  String _surname;
  String _email;
  String _country;
  String _index;
  String _region;
  String _sex;

  List<Step> get steps =>
      [
        Step(
          title: Text('Contact', style: TextStyle(color: Colors.white),),
          isActive: true,

          content: Column(
            children: <Widget>[

//              // Ввод значения _name (Имя)
//              Theme(
//                data: Theme.of(context).copyWith(
//                  unselectedWidgetColor: Colors.white,
//                ),
//                child: new Container(
//                  padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10.00),
//                  child:    TextField(
//                    autofocus: true,
//                    style: TextStyle(color: Colors.white),
//                    maxLines: null, // ne
//                    keyboardType: TextInputType.text,
//                    decoration: InputDecoration(
//                      errorText: "",
//                      errorStyle: TextStyle(
//                          color: Colors.red, fontWeight: FontWeight.bold),
//                      enabledBorder: UnderlineInputBorder(
//                        borderSide: BorderSide(color: Colors.red),
//                      ),
//                      disabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(
//                          color: Colors.white,//this has no effect
//                        ),
//                      ),
//
//                      focusColor: Colors.red,
//
//                      fillColor: Colors.red,
//                      hoverColor: Colors.red,
//                      border: OutlineInputBorder(),
//                      labelText: 'Name',
//                      labelStyle:  TextStyle(color: Colors.white,),
//                    ),
//                    onChanged: (value) async {
//                      _name = value;
//                    },
//                  ),
//                ),
//              ),

              // Блок ввода данных name (Имя)
              Theme(
                data: Theme.of(context).copyWith(
                  unselectedWidgetColor: Colors.white,
                ),
                child: new Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child:    TextFormField(
                    autofocus: false,

                    style: TextStyle(color: Colors.white),
                    maxLines: null, // ne
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: 'First name',
                      errorStyle: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,//this has no effect
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,
                          )
                      ),

                      focusColor: Colors.white,

                      fillColor: Colors.white,
                      hoverColor: Colors.white,
                      border: OutlineInputBorder(),
                      // Вывод данных индекса пользователя в блок до нажатия
                      //hintText: "USA",

                      //counterText: _index,
                      //suffixText: _index,
                      //helperText: _name,
                      //semanticCounterText: _name,
                      labelStyle:  TextStyle(color: Colors.white,),
                    ),
                    onChanged: (value) async {
                      _name = value;
                    },
                    //initialValue: _country,
                  ),
                ),
              ),

              SizedBox(height: 15,),

//              // Ввод значения _name (Фамилия)
//              Theme(
//                data: Theme.of(context).copyWith(
//                  unselectedWidgetColor: Colors.white,
//                ),
//                child: new Container(
//                  padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10.00),
//                  child:    TextField(
//                    autofocus: true,
//                    style: TextStyle(color: Colors.white),
//                    maxLines: null, // ne
//                    keyboardType: TextInputType.text,
//                    decoration: InputDecoration(
//                      errorText: "",
//                      errorStyle: TextStyle(
//                          color: Colors.red, fontWeight: FontWeight.bold),
//                      enabledBorder: UnderlineInputBorder(
//                        borderSide: BorderSide(color: Colors.red),
//                      ),
//                      disabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(
//                          color: Colors.white,//this has no effect
//                        ),
//                      ),
//
//                      focusColor: Colors.red,
//
//                      fillColor: Colors.red,
//                      hoverColor: Colors.red,
//                      border: OutlineInputBorder(),
//                      labelText: 'Surname',
//                      labelStyle:  TextStyle(color: Colors.white,),
//                    ),
//                    onChanged: (value) async {
//                      _surname = value;
//                    },
//                  ),
//                ),
//              ),

              // Блок ввода данных _surname (Фамилия)
              Theme(
                data: Theme.of(context).copyWith(
                  unselectedWidgetColor: Colors.white,
                ),
                child: new Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child:    TextFormField(
                    autofocus: false,

                    style: TextStyle(color: Colors.white),
                    maxLines: null, // ne
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: 'Second name',
                      errorStyle: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,//this has no effect
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,
                          )
                      ),

                      focusColor: Colors.white,

                      fillColor: Colors.white,
                      hoverColor: Colors.white,
                      border: OutlineInputBorder(),
                      // Вывод данных индекса пользователя в блок до нажатия
                      //hintText: "USA",

                      //counterText: _index,
                      //suffixText: _index,
                      //helperText: _name,
                      //semanticCounterText: _name,
                      labelStyle:  TextStyle(color: Colors.white,),
                    ),
                    onChanged: (value) async {
                      _surname = value;
                    },
                    //initialValue: _country,
                  ),
                ),
              ),

              SizedBox(height: 15,),

//              // Ввод значения __email (Почтовый адрес)
//              Theme(
//                data: Theme.of(context).copyWith(
//                  unselectedWidgetColor: Colors.white,
//                ),
//                child: new Container(
//                  padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10.00),
//                  child:    TextField(
//                    autofocus: true,
//                    style: TextStyle(color: Colors.white),
//                    maxLines: null, // ne
//                    keyboardType: TextInputType.text,
//                    decoration: InputDecoration(
//                      errorText: "Input correct Email",
//                      errorStyle: TextStyle(
//                          color: Colors.red, fontWeight: FontWeight.bold),
//                      enabledBorder: UnderlineInputBorder(
//                        borderSide: BorderSide(color: Colors.red),
//                      ),
//                      disabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(
//                          color: Colors.white,//this has no effect
//                        ),
//                      ),
//
//                      focusColor: Colors.red,
//
//                      fillColor: Colors.red,
//                      hoverColor: Colors.red,
//                      border: OutlineInputBorder(),
//                      labelText: 'Email',
//                      labelStyle:  TextStyle(color: Colors.white,),
//                    ),
//                    onChanged: (value) async {
//                      _email = value;
//                    },
//                  ),
//                ),
//              ),

              // Блок ввода данных _email
              Theme(
                data: Theme.of(context).copyWith(
                  unselectedWidgetColor: Colors.white,
                ),
                child: new Container(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child:    TextFormField(
                    autofocus: false,

                    style: TextStyle(color: Colors.white),
                    maxLines: null, // ne
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      labelText: 'Email',
                      errorStyle: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                      ),
                      disabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,//this has no effect
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,
                          )
                      ),

                      focusColor: Colors.white,

                      fillColor: Colors.white,
                      hoverColor: Colors.white,
                      border: OutlineInputBorder(),
                      // Вывод данных индекса пользователя в блок до нажатия
                      //hintText: "email@email.ru",

                      //counterText: _index,
                      //suffixText: _index,
                      //helperText: _name,
                      //semanticCounterText: _name,
                      labelStyle:  TextStyle(color: Colors.white,),
                    ),
                    onChanged: (value) async {
                      _email = value;
                    },
                    //initialValue: _country,
                  ),
                ),
              ),

              SizedBox(height: 15,),
            ],
          ),
        ),

        Step(
          title: Text('Information', style: TextStyle(color: Colors.white),),
          content: Column(
              children: <Widget>[

                // Блок вывода типа пользователя
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Colors.white,
                  ),
                  child: new Container(
                    child: DropDownFormField(
                      titleText: 'You are...',
                      hintText: 'Select one',
                      value: _kind,
                      onSaved: (value) {
                        setState(() {
                          _kind = value;
                        });
                      },
                      onChanged: (value) {
                        setState(() {
                          _kind = value;
                        });
                      },
                      dataSource: [
                        {
                          "display": "Entity",
                          "value": "entity",
                        },
                        {
                          "display": "Individual",
                          "value": "individ",
                        },
                        {
                          "display": "Agent",
                          "value": "agent",
                        },

                      ],
                      textField: 'display',
                      valueField: 'value',
                    ),
                  ),
                ),

                SizedBox(height: 15),

                // Блок ввода данных _country
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Colors.white,
                  ),
                  child: new Container(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child:    TextFormField(
                      autofocus: false,

                      style: TextStyle(color: Colors.white),
                      maxLines: null, // ne
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        labelText: 'Country',
                        errorStyle: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,//this has no effect
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            )
                        ),

                        focusColor: Colors.white,

                        fillColor: Colors.white,
                        hoverColor: Colors.white,
                        border: OutlineInputBorder(),
                        // Вывод данных индекса пользователя в блок до нажатия
                        hintText: "USA",

                        //counterText: _index,
                        //suffixText: _index,
                        //helperText: _name,
                        //semanticCounterText: _name,
                        labelStyle:  TextStyle(color: Colors.white,),
                      ),
                      onChanged: (value) async {
                        _country = value;
                      },
                      //initialValue: _country,
                    ),
                  ),
                ),

                SizedBox(height: 15,),

                // Ввод значения _country (Название страны)
//                Theme(
//                  data: Theme.of(context).copyWith(
//                    unselectedWidgetColor: Colors.white,
//                  ),
//                  child: new Container(
//                    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10.00),
//                    child:    TextField(
//                      autofocus: true,
//                      style: TextStyle(color: Colors.white),
//                      maxLines: null, // ne
//                      keyboardType: TextInputType.emailAddress,
//                      decoration: InputDecoration(
//                        errorText: "",
//                        errorStyle: TextStyle(
//                            color: Colors.red, fontWeight: FontWeight.bold),
//                        enabledBorder: UnderlineInputBorder(
//                          borderSide: BorderSide(color: Colors.red),
//                        ),
//                        disabledBorder: OutlineInputBorder(
//                          borderSide: BorderSide(
//                            color: Colors.white,//this has no effect
//                          ),
//                        ),
//
//                        focusColor: Colors.red,
//
//                        fillColor: Colors.red,
//                        hoverColor: Colors.red,
//                        border: OutlineInputBorder(),
//                        labelText: 'Country',
//                        labelStyle:  TextStyle(color: Colors.white,),
//                      ),
//                      onChanged: (value) async {
//                        _country = value;
//                      },
//                    ),
//                  ),
//                ),

                // Блок ввода данных _region
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Colors.white,
                  ),
                  child: new Container(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child:    TextFormField(
                      autofocus: false,

                      style: TextStyle(color: Colors.white),
                      maxLines: null, // ne
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        labelText: 'Region',
                        errorStyle: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,//this has no effect
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            )
                        ),

                        focusColor: Colors.white,

                        fillColor: Colors.white,
                        hoverColor: Colors.white,
                        border: OutlineInputBorder(),
                        // Вывод данных индекса пользователя в блок до нажатия
                        //hintText: "USA",

                        //counterText: _index,
                        //suffixText: _index,
                        //helperText: _name,
                        //semanticCounterText: _name,
                        labelStyle:  TextStyle(color: Colors.white,),
                      ),
                      onChanged: (value) async {
                        _region = value;
                      },
                      //initialValue: _country,
                    ),
                  ),
                ),

                SizedBox(height: 15,),

//                // Ввод значения _region (Название страны)
//                Theme(
//                  data: Theme.of(context).copyWith(
//                    unselectedWidgetColor: Colors.white,
//                  ),
//                  child: new Container(
//                    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10.00),
//                    child:    TextField(
//                      autofocus: false,
//                      style: TextStyle(color: Colors.white),
//                      maxLines: null, // ne
//                      keyboardType: TextInputType.emailAddress,
//                      decoration: InputDecoration(
//                        errorText: "",
//                        errorStyle: TextStyle(
//                            color: Colors.red, fontWeight: FontWeight.bold),
//                        enabledBorder: UnderlineInputBorder(
//                          borderSide: BorderSide(color: Colors.red),
//                        ),
//                        disabledBorder: OutlineInputBorder(
//                          borderSide: BorderSide(
//                            color: Colors.white,//this has no effect
//                          ),
//                        ),
//
//                        focusColor: Colors.red,
//
//                        fillColor: Colors.red,
//                        hoverColor: Colors.red,
//                        border: OutlineInputBorder(),
//                        labelText: 'Region',
//                        labelStyle:  TextStyle(color: Colors.white,),
//                      ),
//                      onChanged: (value) async {
//                        _region = value;
//                      },
//                    ),
//                  ),
//                ),

                // Блок ввода данных _index
                Theme(
                  data: Theme.of(context).copyWith(
                    unselectedWidgetColor: Colors.white,
                  ),
                  child: new Container(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child:    TextFormField(
                      autofocus: false,

                      style: TextStyle(color: Colors.white),
                      maxLines: null, // ne
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        labelText: 'ZIP/Postal Code',
                        errorStyle: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                        ),
                        disabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.white,//this has no effect
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.white,
                            )
                        ),

                        focusColor: Colors.white,

                        fillColor: Colors.white,
                        hoverColor: Colors.white,
                        border: OutlineInputBorder(),
                        // Вывод данных индекса пользователя в блок до нажатия
                        //hintText: "USA",

                        //counterText: _index,
                        //suffixText: _index,
                        //helperText: _name,
                        //semanticCounterText: _name,
                        labelStyle:  TextStyle(color: Colors.white,),
                      ),
                      onChanged: (value) async {
                        _index = value;
                      },
                      //initialValue: _country,
                    ),
                  ),
                ),

                SizedBox(height: 20,),

//                // Ввод значения _index (Название страны)
//                Theme(
//                  data: Theme.of(context).copyWith(
//                    unselectedWidgetColor: Colors.white,
//                  ),
//                  child: new Container(
//                    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10.00),
//                    child:    TextField(
//                      autofocus: true,
//                      style: TextStyle(color: Colors.white),
//                      maxLines: null, // ne
//                      keyboardType: TextInputType.emailAddress,
//                      decoration: InputDecoration(
//                        errorText: "",
//                        errorStyle: TextStyle(
//                            color: Colors.red, fontWeight: FontWeight.bold),
//                        enabledBorder: UnderlineInputBorder(
//                          borderSide: BorderSide(color: Colors.red),
//                        ),
//                        disabledBorder: OutlineInputBorder(
//                          borderSide: BorderSide(
//                            color: Colors.white,//this has no effect
//                          ),
//                        ),
//
//                        focusColor: Colors.red,
//
//                        fillColor: Colors.red,
//                        hoverColor: Colors.red,
//                        border: OutlineInputBorder(),
//                        labelText: 'ZIP/Postal Code',
//                        labelStyle:  TextStyle(color: Colors.white,),
//                      ),
//                      onChanged: (value) async {
//                        _index = value;
//                      },
//                    ),
//                  ),
//                ),
              ]
          ),

          isActive: true,
        ),
      ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: Colors.grey[800],
      body:Theme(data: ThemeData( primaryColor: Colors.red),
        child: ListView(
          children: <Widget>[
            // Название блок "Registration"
            Container(
              padding: const EdgeInsets.fromLTRB(30, 30, 0, 0),
              child: Text(
                "REGISTRATION", textAlign: TextAlign.left, style: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.w300,
              ),
              ),
            ),

            SizedBox(height: 20,),

            // Разделяющий блокк
            Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
              margin: const EdgeInsets.only(left: 10.0, right: 10.0),),

            Stepper(
                currentStep: this.current_step,
                steps: steps,
                type: StepperType.vertical,
                onStepTapped: (step) {
                  setState(() {
                    current_step = step;
                  });
                },
                onStepContinue: () {
                  setState(() {
                    print(current_step);
                    print(_name);
                    print(_surname);
                    print(_email);
                    // Шаг заполнения данных контакт
                    if (current_step == 0 && _name != null && _surname != null && _email != null)
                    {
                      current_step += 1;
                    }
                    else if(current_step == 1 && _name != null && _surname != 0 && _email != null && _kind != null
                        && _country != null && _region != null && _index != null)
                    {
                      createUser();
                      current_step += 1;
                    }
                    else{
                      current_step = 0;
                    }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if (current_step > 0) {
                      current_step = current_step - 1;
                    } else {
                      current_step = 0;
                    }
                  });
                },
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  return Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        onPressed: onStepCancel,
                        child: const Text('Previous', style: TextStyle(color: Colors.white),),
                      ),

                      FlatButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(5.0)),
                        onPressed: onStepContinue,
                        child: const Text('Next'),
                      ),


                    ],
                  );
                }),
          ]
        )
      ),


    );


  }

  Future<void> createUser() async {
    firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true, provisional: false),
    );
    SharedPreferences.getInstance().then((SharedPreferences prefs) async {
      prefs.setString('uid', UserSettings.UID);
      print("set uid of prefs: " + prefs.getString('uid'));
      String _token = await firebaseMessaging.getToken();
      print("Ladding: " + 'received token');
      
   await FBManager.addNewUser({
      'name': _name,
      'phone': prefs.getString('phone'),
      'surname': _surname,
      'role' : 'empl',
      'device_token' : _token,
      'notifications_of_acceptance' : true,
      'token' : uid,
     'email':_email,
     'country':_country,
     'index':_index,
     'region':_region,
   },);
    });
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => MyApp()));
  }

  }

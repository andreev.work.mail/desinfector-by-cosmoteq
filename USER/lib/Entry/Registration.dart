import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import '../DataBaseAccess/FBManager.dart';
import 'Ladding.dart';
import '../main.dart';
import 'package:flutter/cupertino.dart';
import 'package:regexed_validator/regexed_validator.dart';
import '../Settings/userSettings.dart';
import '../Settings/userSettings.dart';

enum GenderList { male, female }

class MyForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyFormState();
}

class MyFormState extends State {
  final _formKey = GlobalKey<FormState>();
  GenderList _gender;
  String _name;
  String _phoneNum;
  String verifCode;
  String verifId;
  String smsCode;
  bool _agreement = false;

  Future<void> redirectUser(String uid) async {
    DocumentSnapshot snap = await FBManager.getUserStats(uid);
    UserSettings.UID = uid;
    UserSettings.phone = _phoneNum;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone', _phoneNum);
    prefs.setString('uid', uid);
    if (snap == null || !snap.exists || snap['role'] != 'empl') {

      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => StepperDemo()));
    } else {
      UserSettings.userDocument = snap;
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (context) => MyApp()));
    }
  }


  Future<void> verifyPhoneNum() async {
    final PhoneCodeAutoRetrievalTimeout autoRetrieve = (String verifId) {
      this.verifId = verifId;
    };
    final PhoneCodeSent smsCodeSent = (String verifId, [int forceCodeResend]) {
      this.verifId = verifId;
      smsCodeDialog(context).then((value) async {
        FBManager.init();
        print("Registration: " + "SignedIn");
        //redirectUser();
      });
    };

    final PhoneVerificationCompleted verifSuccess = (AuthCredential user) {
      print("Registration: " + "Successfully verified!");

      //smsCodeDialog(context);
    };

    final PhoneVerificationFailed verifFailed = (AuthException excetpion) {
      print("Registration: " + '${excetpion.message}');
    };

    await FirebaseAuth.instance.verifyPhoneNumber(phoneNumber: _phoneNum,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verifSuccess,
        verificationFailed: verifFailed,
        codeSent: smsCodeSent,
        codeAutoRetrievalTimeout: autoRetrieve);
  }


  Future<bool> smsCodeDialog(BuildContext context) async {
    return showDialog(context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(16.0))
            ),
            title: Text("SMS code"),
            content: PinCodeTextField(
              length: 6,
              textInputType: TextInputType.number,
              inactiveColor: Colors.grey,
              selectedColor: Colors.red,
              obsecureText: false,
              animationType: AnimationType.fade,
              shape: PinCodeFieldShape.box,
              animationDuration: Duration(milliseconds: 300),
              borderRadius: BorderRadius.circular(5),
              fieldHeight: 40,
              fieldWidth: 30,
              onChanged: (value) {
                setState(() {
                  this.smsCode = value;
                });
              },
            ),

            actions: <Widget>[
              new FlatButton(onPressed: () {
                FirebaseAuth.instance.currentUser().then((user) async {

                  if (user != null) {
                    print("Registration: " + "user phone num: " + user.phoneNumber + " user uid: " + user.uid);
                    UserSettings.UID = user.uid;
                    //Navigator.of(context).pop();
                    FBManager.init();
                    redirectUser(user.uid);

                  }
                  else {
                    signIn(); 
                    Navigator.of(context).pop();
                  }
                });
              },
                  child: Text("Next", style: TextStyle(color: Colors.red),))
            ],
          );
        });
  }

  signIn() {
    final AuthCredential credential = PhoneAuthProvider.getCredential(
      verificationId: verifId,
      smsCode: smsCode,
    );

    FirebaseAuth.instance.signInWithCredential(credential).then((user) async {
      FBManager.init();
      redirectUser(user.user.uid);
    }).catchError((e) {
      print("Registration: " + e);
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Превышен лимит запросов. Попробуйте позже.'),
        backgroundColor: Colors.red,));
    });
  }

  validatePhoneNum(){
    _phoneNum = _phoneNum.trim();
    _phoneNum = _phoneNum.replaceAll('-', '');
    _phoneNum = _phoneNum.replaceAll('(', '');
    _phoneNum = _phoneNum.replaceAll(')', '');
    if(_phoneNum.length == 12 && _phoneNum[0] == "+" && _phoneNum[1] == '7')
      return true;
    if(_phoneNum.length == 11 && _phoneNum[0] == '8') {
      _phoneNum = _phoneNum.replaceFirst('8', '+7');
      print("_phoneNum: " + _phoneNum);
      return true;
    }
    return false;
  }


  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Theme(data: ThemeData(primaryColor: Colors.red),
        child:
        SingleChildScrollView(
            child: new Form(
                key: _formKey,
                child: new Column(
                  children: <Widget>[
                    new Container(

                      child: Center(

                    child:RichText(
                    text: new TextSpan(
                    children: [
                    new TextSpan(
                    text: 'VIRUS',
                      style: new TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    ),
                    new TextSpan(
                      text: 'AROUND ',
                      style: new TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 25),
                    ),



                    ],
                    ),
                ),

            ),
                      height: 130.0,
                      width: 300.0,
                    ),

                    Container(
                      child: Text('Authorization via sms',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0, color: Colors.white),
                      ),
                    ),
                    Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.white,
                      ),
                      child: new Container(
                        padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 10.00),
                        child:    TextField(
                          autofocus: true,
                          style: TextStyle(color: Colors.white),
                          maxLines: null, // ne
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            errorText: "Input correct phone number",
                            errorStyle: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.white,//this has no effect
                              ),
                            ),

                            focusColor: Colors.red,

                            fillColor: Colors.red,
                            hoverColor: Colors.red,
                            border: OutlineInputBorder(),
                            labelText: 'Phone number',
                          ),
                          onChanged: (value) async {
                            _phoneNum = value;
                          },
                        ),
                      ),
                    ),

                    Theme(
                      data: Theme.of(context).copyWith(
                        unselectedWidgetColor: Colors.white,
                      ),
                      child: new Container(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),

                        child:  CheckboxListTile(
                            checkColor: Colors.white,

                            activeColor: Colors.red,
                            value: _agreement,
                            title: new Container(
                              child: new RichText(
                                text: new TextSpan(
                                  children: [
                                    new TextSpan(
                                      text: 'I am familiar with ',
                                      style: new TextStyle(color: Colors.white),
                                    ),
                                    new TextSpan(
                                      text: 'the terms and conditions ',
                                      style: new TextStyle(color: Colors.red, decoration: TextDecoration.underline),
                                      recognizer: new TapGestureRecognizer()
                                        ..onTap = () { launch('http://virusaround.com/');
                                        },
                                    ),
                                    new TextSpan(
                                      text: ' of the service',
                                      style: new TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            onChanged: (bool value) => setState(() => _agreement = value)


                        ),

                      ),
                    ),
                    new Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.00),
                      child:  new RaisedButton(onPressed: () {
                        if (!_agreement)
                          Scaffold.of(context).showSnackBar(SnackBar(
                            content: Text('Чтобы продолжить, примите Соглашение'),
                            backgroundColor: Colors.redAccent,));

                        else if (_formKey.currentState.validate())
                          if(validatePhoneNum()) {
                            verifyPhoneNum();
                            print("Registration: num: " + _phoneNum);
                          }else {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Check your phone number', textAlign: TextAlign.center,),
                              backgroundColor: Colors.redAccent,));
                          }
                      },
                        child: Text('Continue'),
                        color: Colors.red,
                        textColor: Colors.white,),
                    )],))),),);
  }
}


class Regist extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent,
        statusBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark));
    return Scaffold(
//      title: Text('Вход по одноразовому коду',style: TextStyle(color: Colors.black), textAlign: TextAlign.center,)),
      body: new MyForm(),
    );
  }
}


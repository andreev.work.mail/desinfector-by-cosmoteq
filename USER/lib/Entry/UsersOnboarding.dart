//import 'dart:js';

import 'package:flutter/material.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../main.dart';
import 'package:geolocator/geolocator.dart';

import 'Registration.dart';


class OnBoarding extends StatelessWidget {
  //making list of pages needed to pass in IntroViewsFlutter constructor.


  final pages = [

    PageViewModel(
        pageColor: Colors.black,
        // iconImageAssetPath: 'assets/air-hostess.png',
        bubble: Icon(Icons.done),
        body: Text(
          'All disinfection companies in one app help keep you safe from viruses and diseases' , style: TextStyle(color: Colors.white, fontSize: 18),
        ),

        title: Text(
          'Efficiency', style: TextStyle(fontWeight: FontWeight.bold)
        ),
        titleTextStyle: TextStyle(fontSize: 20),
        textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        bubbleBackgroundColor: Colors.red,
        iconColor: Colors.grey,


        mainImage: Image.asset(
          'assets/onb1.jpeg',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
        pageColor: Colors.black,
        // iconImageAssetPath: 'assets/air-hostess.png',
        bubble: Icon(Icons.work),
        body: Text(
            'Many reliable companies are ready to help you deal with potential virus threats' ,  style: TextStyle(color: Colors.white, fontSize: 18)
        ),
        title: Text(
          'Safeness', style: TextStyle(fontWeight: FontWeight.bold),
        ),
        titleTextStyle: TextStyle(fontSize: 20),
        textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        bubbleBackgroundColor: Colors.red,
        iconColor: Colors.grey,
        mainImage: Image.asset(
          'assets/onb2.jpeg',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(

        pageColor: Colors.black,
        // iconImageAssetPath: 'assets/air-hostess.png',
        bubble: Icon(Icons.monetization_on),
        body: Text(
            'Here begins your clean and happy life' , style: TextStyle(color: Colors.white, fontSize: 18)
        ),
        title: Text(
          'Welcome',  style: TextStyle(fontWeight: FontWeight.bold)
        ),
        titleTextStyle: TextStyle(fontSize: 20),
        textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        bubbleBackgroundColor: Colors.red,
        iconColor: Colors.grey,
        mainImage: Image.asset(
          'assets/onb3.jpg',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),



  ];

  @override
  Widget build(BuildContext context) {
    _incrementCounter();

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent,
        statusBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark
    ));
    return MaterialApp(

      debugShowCheckedModeBanner: false,
      home: Builder(
        builder: (context) => IntroViewsFlutter(
          pages,
          onTapDoneButton: () {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(

                builder: (context) => Regist(),
              ), //MaterialPageRoute
            );
          },
          showSkipButton: false, //Whether you want to show the skip button or not.
          doneText: Text("Next"),
          fullTransition: 150.0,
          pageButtonTextStyles: TextStyle(
            color: Colors.red,
            fontSize: 18.0,
          ),
        ), //IntroViewsFlutter
      ), //Builder
    ); //Material App

  }
}
_incrementCounter() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  int check = (prefs.getInt('check') ?? 1) ;
  print('$check');
  await prefs.setInt('check', check);
}



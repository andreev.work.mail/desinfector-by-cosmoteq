import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:virusaround_admin/screens/home_screen.dart';
import 'package:virusaround_admin/screens/login_screen.dart';

class AuthService {
  //Handle Authentication
  Map<dynamic, dynamic> UserInfo;

  handleAuth() {
    return StreamBuilder(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (context, snaphot) {
        if (snaphot.hasData) {
          print( snaphot.data.toString());


          return HomePage();
        } else {
          return LoginPage();
        }
      },
    );
  }

  //Sign Out
  signOut() {
    FirebaseAuth.instance.signOut();
  }

  //Sign in
  signIn(email, password) {
    FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password)
        .then((user) {
      print('Signed in');
    }).catchError((e) {
      print(e);
    });
  }

  register(email, password) {
    FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((user) {
      print('reg - ok!');
    }).catchError((e) {
      print(e);
    });
  }

}
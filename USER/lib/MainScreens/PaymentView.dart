import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'LogsView.dart';
import 'ProfileView.dart';

class GetCurrentURLWebView extends StatefulWidget {
  String url;
  GetCurrentURLWebView(String url){
    this.url = url;
  }
  @override
  GetCurrentURLWebViewState createState() {
    Logs.log += "\nTO STATE\n";
    return new GetCurrentURLWebViewState(url);
  }
}

class GetCurrentURLWebViewState extends State<GetCurrentURLWebView> {
  InAppWebViewController webView;
  String url;

  GetCurrentURLWebViewState(String url){
    Logs.log += "\nCONSTRUCTING\n";
    this.url = url;
  }

  setUrl(String url){
    this.url = url;
  }

  parseHtml(var context) async{
    Logs.log += "\nPARCING\n";
    Logs.log += "\n" + (await webView.getHtml()).toString() + "\n";
    bool confirmed = (await webView.getHtml()).contains("Заказ успешно оплачен");
    print("parsed: " + confirmed.toString());
    if(confirmed)
      Scaffold.of(context).showSnackBar(SnackBar(
        content:
        Text('Успешно'),
        backgroundColor: Colors.red,
      ));
    Navigator.of(context).pop();
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    Logs.log += "\nBUILDING\n";
    changedBalance = true;
//    url = getTok();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Пополение баланса'),
        backgroundColor: Colors.red,

      ),
      body: Container(
          child: Column(children: <Widget>[
            Expanded(
              child: Container(
                margin: const EdgeInsets.all(10.0),
                decoration:
                BoxDecoration(border: Border.all(color: Colors.red)),
                child: InAppWebView(
                  initialUrl: url,
                  initialHeaders: {},
                  initialOptions: InAppWebViewWidgetOptions(),
                  onWebViewCreated: (InAppWebViewController controller) {
                    Logs.log += "\nWEBVIEW CONTROLLER CREATED\n";
                    webView = controller;
                  },
                  onLoadStart: (InAppWebViewController controller, String _url) {
                    setState(() {
                      Logs.log += "\nONLOAD STARTED\n";
                      url = _url;
                    });
                  },
                  onLoadStop:
                      (InAppWebViewController controller, String _url) async {
                    setState(() {
                      url = _url;
                      Logs.log += "\nONLOAD STOP URL: " + url + "\n";
                      print(url);
                      if(url.contains("BANKOCEAN2R")){
                        //parseHtml(context);
                        print("PARCING");

                      }

                    });
                  },
                  onProgressChanged:
                      (InAppWebViewController controller, int progress) {
                  },
                ),
              ),
            ),
         ])),
    );
  }
}

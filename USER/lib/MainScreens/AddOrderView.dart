import 'dart:ffi';

import 'package:chips_choice/chips_choice.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_map_location_picker/generated/i18n.dart'
    as location_picker;
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../DataBaseAccess/FBManager.dart';
import '../Settings/userSettings.dart';
import 'LogsView.dart';
import 'ProfileView.dart';

String uid;

class AddPostStep extends StatefulWidget {
  @override
  AddPostStepState createState() => AddPostStepState();
}

class AddPostStepState extends State<AddPostStep> {
  TextEditingController controlAddress = TextEditingController();

  LocationResult _pickedLocation;

  Position _currentPosition;

  String _currentAddress;
  String _description;
  String _title;
  String _contact;
  String _ScheduleType;
  String _workType;

  Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  TextEditingController dateCtl = TextEditingController();
  TextEditingController dateCtl2 = TextEditingController();

  Timestamp startTime;
  Timestamp endTime;

  final format = DateFormat("dd-MM-yyyy HH:mm");

  double _lowerValue = 50;
  double _upperValue = 180;
  double _latitude;
  double _longitude;

  int minPrice = 10;
  int maxPrice = 3000;
  int minSquere = 3;
  int maxSquere = 3000;
  int current_step = 0;

  var _time = new DateTime.now();
  bool _showContacts = false;


  String isAddressSet() {
    if (_currentAddress == null) return '...';
    return _currentAddress;
  }

  DateTime date;
  TimeOfDay time;

  Future<String> getUid() async {
    FirebaseAuth.instance.currentUser().then((user) {
      uid = user.uid;
      return user.uid;
    });
  }

  Future<Void> setOrder() async {
    if (UserSettings.UID == null) await getUid();
    uid = UserSettings.UID;
  }

  String _price;

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p =
          await geolocator.placemarkFromCoordinates(_latitude, _longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress =
            "${place.locality}, ${place.thoroughfare}, ${place.name}";
      });
    } catch (e) {
      print("AddPost: " + e);
    }
  }

  String checkTimeSet(Timestamp timestamp) {
    if (timestamp == null) return "Добавьте время";
    return timestamp.toDate().toLocal().toString();
  }

  List<Step> get steps => [
    // Блок ввода основной информации о задаче
    Step(
      // Название первого шага заполения задачи
      title: Text(
        'Where will the disinfection be?',
        style: TextStyle(color: Colors.white),
      ),
      isActive: true,
      content: Column(
        children: <Widget>[

          SizedBox(
            height: 5,
          ),

          // Блок ввода данных _title (Название вакансии)
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.white,
            ),
            child: new Container(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child:    TextFormField(
                autofocus: false,

                style: TextStyle(color: Colors.white),
                maxLines: null, // ne
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Task name',
                  errorStyle: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,//this has no effect
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white,
                      )
                  ),

                  focusColor: Colors.white,

                  fillColor: Colors.white,
                  hoverColor: Colors.white,
                  border: OutlineInputBorder(),
                  // Вывод данных индекса пользователя в блок до нажатия
                  hintText: "Coronavirus disinfection",

                  //counterText: _index,
                  //suffixText: _index,
                  //helperText: _name,
                  //semanticCounterText: _name,
                  labelStyle:  TextStyle(color: Colors.white,),
                ),
                onChanged: (value) async {
                  _title = value;
                },
                initialValue: _title,
              ),
            ),
          ),

          SizedBox(
            height: 15,
          ),

          // Блок выбора типа помещений дезинфекции
          Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: Colors.white,
          ),
          child: new Container(
            child: DropDownFormField(
              titleText: 'What needs to be sanitized?',
              hintText: 'Select the type',
              value: _workType,
              onSaved: (value) {
                setState(() {
                  _workType = value;
                });
              },
              onChanged: (value) {
                setState(() {
                  _workType = value;
                });
              },
              dataSource: [
                {
                  "display": "Apartment / room",
                  "value": "Apartment / room",
                },
                {
                  "display": "Private house",
                  "value": "Private house",
                },
                {
                  "display": "Office space",
                  "value": "Office space",
                },
                {
                  "display": "Commercial premises",
                  "value": "Commercial premises",
                },
                {
                  "display": "Street space",
                  "value": "Street space",
                },
                {
                  "display": "Another variant",
                  "value": "Another variant",
                }
              ],
              textField: 'display',
              valueField: 'value',
            ),
          ),
        ),

          SizedBox(height: 10),

          // Блок ввода данных _description (Описание вакансии)
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.white,
            ),
            child: new Container(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child:    TextFormField(
                autofocus: false,

                style: TextStyle(color: Colors.white),
                maxLines: null, // ne
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  labelText: 'Description',
                  errorStyle: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,//this has no effect
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white,
                      )
                  ),

                  focusColor: Colors.white,

                  fillColor: Colors.white,
                  hoverColor: Colors.white,
                  border: OutlineInputBorder(),
                  // Вывод данных индекса пользователя в блок до нажатия
                  //hintText: _title,

                  //counterText: _index,
                  //suffixText: _index,
                  //helperText: _name,
                  //semanticCounterText: _name,
                  labelStyle:  TextStyle(color: Colors.white,),
                ),
                onChanged: (value) async {
                  _description = value;
                },
                initialValue: _description,
              ),
            ),
          ),

          SizedBox(
            height: 10,
          ),

          // Блок подробного описания задачи
          Container(
            child: Text(
              "Describe in detail the task and wishes for disinfection.",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.grey),
            ),
          ),

          SizedBox(
            height: 40,
          ),

          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.white,
            ),
            child: new Container(
              child: DropDownFormField(
                titleText: 'Schedule',
                hintText: 'Select the schedule',
                value: _ScheduleType,
                onSaved: (value) {
                  setState(() {
                    _ScheduleType = value;
                  });
                },
                onChanged: (value) {
                  setState(() {
                    _ScheduleType = value;
                  });
                },
                dataSource: [
                  {
                    "display": "Job-work",
                    "value": "Job-work",
                  },
                  {
                    "display": "Permanency",
                    "value": "Permanency",
                  },
                ],
                textField: 'display',
                valueField: 'value',
              ),
            ),
          ),

          SizedBox(height: 10),

          // Блок ввода площади помещений
          Container(
              child: FlutterSlider(
            values: [300, 1500],
            rangeSlider: true,
            max: 3000,
            step: 100,
            min: 100,
            onDragging: (handlerIndex, lowerValue, upperValue) {
              _lowerValue = lowerValue;
              _upperValue = upperValue;

              setState(() {
                minSquere = _lowerValue.toInt();
                maxSquere = _upperValue.toInt();
              });
            },
          )),

          SizedBox(height: 10),

          // Текстовое поле вывода выбранной площади
          Text(
            'from ' +
                minSquere.toString() +
                'm²' +
                ' to ' +
                maxSquere.toString() +
                'm²',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
          ),

          SizedBox(height: 25),

          // Текстовое поле с комментарием по выбора площади помещения
          Text(
            'Indicate the approximate area  to be sanitized.',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),

          SizedBox(height: 10),
        ],
      ),
    ),

    // Блок ввода адрес выполнения заказа
    Step(
      title: Text('Where to perform the task?',
          style: TextStyle(color: Colors.white)),
      content: Column(children: <Widget>[

        // Блок ввода данных _title (Название вакансии)
        Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: Colors.white,
          ),
          child: new Container(
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child:    TextFormField(
              autofocus: false,
              controller: controlAddress,
              style: TextStyle(color: Colors.white),
              maxLines: null, // ne
              keyboardType: TextInputType.text,
              onTap:() async{
                FocusScope.of(context).requestFocus(new FocusNode());

                print("AddPost: " + 'calling geo');
                LocationResult result = await showLocationPicker(
                context,
                "AIzaSyBqek-AR01ftmNzcrsZkIWsqL1N33ygP0A",
                //                      mapStylePath: 'assets/mapStyle.json',
                myLocationButtonEnabled: true,
                layersButtonEnabled: true,
                //                      resultCardAlignment: Alignment.bottomCenter,
                );
                print("AddPost: " + "result = $result");
                setState(() => _pickedLocation = result);
                _latitude = _pickedLocation.latLng.latitude;
                _longitude = _pickedLocation.latLng.longitude;

                // Обновление данных _currentAddress
                await _getAddressFromLatLng();

                // Обновление поля ввыода данных
                setState(() {
                  // Обновление данных поля
                  controlAddress.text = isAddressSet();
                });
              } ,
              decoration: InputDecoration(
                labelText: 'Indicate your address',
                suffixIcon: Icon(Icons.arrow_right),
                errorStyle: TextStyle(
                    color: Colors.white, fontWeight: FontWeight.bold),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                disabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.white,//this has no effect
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Colors.white,
                    )
                ),

                focusColor: Colors.white,

                fillColor: Colors.white,
                hoverColor: Colors.white,
                border: OutlineInputBorder(),
                // Вывод данных индекса пользователя в блок до нажатия
                hintText: "Tap to select",

                //counterText: _index,
                //suffixText: _index,
                //helperText: _name,
                //semanticCounterText: _name,
                labelStyle:  TextStyle(color: Colors.white,),
              ),
              onChanged: (value) async {
                _title = value;
              },
              //initialValue: _title,
            ),
          ),
        ),

        SizedBox(height: 10),
      ]),
      isActive: true,
    ),

    // Шаг выбора оплаты
    Step(
          //
          title: Text('Set up payment', style: TextStyle(color: Colors.white)),
          content: Column(children: <Widget>[

            SizedBox(height: 24),

            // Поле ввода провежутка размера оплаты труда
            Container(
                child: FlutterSlider(
                  values: [300, 1500],
                  rangeSlider: true,
                  max: 3000,
                  step: 100,
                  min: 10,
                  onDragging: (handlerIndex, lowerValue, upperValue) {
                    _lowerValue = lowerValue;
                    _upperValue = upperValue;

                    setState(() {
                      minPrice = _lowerValue.toInt();
                      maxPrice = _upperValue.toInt();
                    });
                  },
                )),

            SizedBox(height: 10),

            // Вывод промежутка выбранной соимости оплаты работы
            Text(
              'from ' +
                  minPrice.toString() +
                  '\$' +
                  ' to ' +
                  maxPrice.toString() +
                  '\$',
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),

            SizedBox(height: 25),

            // Текстовый комментарий по заполнению поля
            Text(
              'Indicate how much you are going to give for the work performed.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),

            SizedBox(height: 10),
          ]),
          isActive: true,
        ),

    // Шаг ввода контактного номера телефона
    Step(
          // Название четвертого щага размещения задачи
          title: Text('What is your contact phone?',
              style: TextStyle(color: Colors.white)),
          content: Column(children: <Widget>[

            SizedBox(height: 10),

            // Блок ввода данных _contact (Описание вакансии)
            Theme(
              data: Theme.of(context).copyWith(
                unselectedWidgetColor: Colors.white,
              ),
              child: new Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                child:    TextFormField(
                  autofocus: false,

                  style: TextStyle(color: Colors.white),
                  maxLines: null, // ne
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Phone number',
                    errorStyle: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.white,//this has no effect
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,
                        )
                    ),

                    focusColor: Colors.white,

                    fillColor: Colors.white,
                    hoverColor: Colors.white,
                    border: OutlineInputBorder(),
                    // Вывод данных индекса пользователя в блок до нажатия
                    hintText: "+1(123)-123-1234",

                    //counterText: _index,
                    //suffixText: _index,
                    //helperText: _name,
                    //semanticCounterText: _name,
                    labelStyle:  TextStyle(color: Colors.white,),
                  ),
                  onChanged: (value) async {
                    _contact = value;
                  },
                  initialValue: _contact,
                ),
              ),
            ),

            SizedBox(height: 10,),

            Text(
              'Please enter a valid phone number to increase ad relevance.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.grey),
            ),
            SizedBox(height: 10),
          ]),
          isActive: true,
        ),
  ];

  String _sex;

  static String _work;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        darkTheme: ThemeData(
          brightness: Brightness.dark,
        ),
        theme: ThemeData(
          //
          primarySwatch: Colors.red,
        ),
        debugShowCheckedModeBanner: false,
        localizationsDelegates: const [
          location_picker.S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: const <Locale>[
          Locale('ru'),
        ],
        home: Scaffold(
          backgroundColor: Colors.grey[800],
          body: Theme(
            data: ThemeData(primaryColor: Colors.red),
            child: Stepper(
                currentStep: this.current_step,
                steps: steps,
                type: StepperType.vertical,
                onStepTapped: (step) {
                  setState(() {
                    current_step = step;
                  });
                },
                onStepContinue: () {
                  setState(() {
                    if ((minPrice != null && _contact != null && _currentAddress != null &&
                        _title != null &&
                        _description != null && minSquere != null && _workType != null &&
                        _ScheduleType != null && _description != null)) {
                      setOrder();
                      Logs.log += "\n\nENTERING\n\n";
                      changedBalance = true;
                      FBManager.addOrder({
                        'title': _title,
                        'min_price': minPrice,
                        'max_price': maxPrice,
                        'min_squere': minSquere,
                        'max_squere': maxSquere,
                        'description': _description,
                        'address': _currentAddress,
                        'geopos': Geoflutterfire()
                            .point(latitude: _latitude, longitude: _longitude)
                            .data,
                        'token': uid,
                        'company': [],
                        'phone_number': _contact,
                        'schedule_type': _ScheduleType,
                        'work_type': _workType,
                        'date': ('${_time.day}.${_time.month}.${_time.year}').toString(),
                        'number_of_views':0,
                        'first_name':UserSettings.userDocument['name'],
                        'second_name':UserSettings.userDocument['surname'],
                        'email':UserSettings.userDocument['email'],
                      }, UserSettings.userDocument)
                          .then((val) {
                        Navigator.of(context).pop();
                      });
                    }
                    else if (current_step < steps.length - 1)
                    {
                      print(_title.toString() + _description.toString()+ minSquere.toString() + _ScheduleType.toString()+ _workType.toString());

                      if (current_step == 0 &&
                          _title != null &&
                          _description != null && minSquere != null && _workType != null &&
                          _ScheduleType != null && _description != null)
                      {
                        current_step = current_step + 1;
                        print("AddPost: " + current_step.toString());
                      }
                      else if (current_step == 1 && _currentAddress != null &&
                          _title != null &&
                          _description != null && minSquere != null && _workType != null &&
                          _ScheduleType != null && _description != null) {
                        current_step = current_step + 1;

                        print("AddPost: " + current_step.toString());
                      }
                      else if (current_step == 3 && _contact != null && _currentAddress != null &&
                          _title != null &&
                          _description != null && minSquere != null && _workType != null &&
                          _ScheduleType != null && _description != null)
                      {
                        current_step = current_step + 1;
                        print("AddPost: " + current_step.toString());
                      }
                      else if (current_step == 2 && minPrice != null && _currentAddress != null &&
                          _title != null &&
                          _description != null && minSquere != null && _workType != null &&
                          _ScheduleType != null && _description != null)
                      {
                        current_step = current_step + 1;
                      }
                    }
                    else
                      {
                      current_step = 0;
                    }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if (current_step > 0) {
                      current_step = current_step - 1;
                    } else {
                      current_step = 0;
                    }
                  });
                },
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  return Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      FlatButton(
                        onPressed: onStepCancel,
                        child: const Text('Previous', style: TextStyle(color: Colors.white),),
                      ),
                      FlatButton(
                        color: Colors.red,
                        textColor: Colors.white,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5.0)),
                        onPressed: onStepContinue,
                        child: const Text('Next'),
                      ),
                    ],
                  );
                }),
          ),
        ));
  }
}

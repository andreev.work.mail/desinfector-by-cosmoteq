import 'package:flutter/material.dart';
import 'package:virusaround_admin/service/auth.dart';


class RegistPage extends StatefulWidget {
  @override
  _RegistPagePageState createState() => _RegistPagePageState();
}

class _RegistPagePageState extends State<RegistPage> {
  String email, password;

  final formKey = new GlobalKey<FormState>();

  checkFields() {
    final form = formKey.currentState;
    if (form.validate()) {
      return true;
    } else {
      return false;
    }
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Container(
                height: 600.0,
                width: 500.0,
                child: Column(
                  children: <Widget>[
                    Form(
                        key: formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[

                            Container(
                              child: RichText(
                                text: new TextSpan(
                                  children: [
                                    new TextSpan(
                                      text: 'VIRUS',
                                      style: new TextStyle(color: Colors.red , fontWeight: FontWeight.bold, fontSize: 40),
                                    ),
                                    new TextSpan(
                                      text: 'AROUND ',
                                      style: new TextStyle(color: Colors.white , fontWeight: FontWeight.bold, fontSize: 40),
                                    ),
                                    new TextSpan(
                                      text: 'Админ панель',
                                      style: new TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(height: 100,),

                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0,
                                    right: 25.0,
                                    top: 20.0,
                                    bottom: 5.0),
                                child: Container(
                                  height: 50.0,
                                  child: TextFormField(
                                    decoration:
                                    InputDecoration(hintText: 'Email'),
                                    validator: (value) => value.isEmpty
                                        ? 'Email is required'
                                        : validateEmail(value.trim()),
                                    onChanged: (value) {
                                      this.email = value;
                                    },
                                  ),
                                )),

                            Padding(
                                padding: EdgeInsets.only(
                                    left: 25.0,
                                    right: 25.0,
                                    top: 20.0,
                                    bottom: 5.0),
                                child: Container(
                                  height: 50.0,
                                  child: TextFormField(
                                    obscureText: true,
                                    decoration:
                                    InputDecoration(hintText: 'Пароль'),
                                    validator: (value) => value.isEmpty
                                        ? 'Password is required'
                                        : null,
                                    onChanged: (value) {
                                      this.password = value;
                                    },
                                  ),
                                )),
                            SizedBox(height: 40,),
                            InkWell(
                                onTap: () {
                                  if (checkFields()) {
                                    AuthService().register(email, password);
                                    Navigator.pop(context);
                                  }
                                },
                                child: Container(
                                    height: 50.0,
                                    width: 100.0,
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                    ),
                                    child: Center(child: Text('Зарегистрироваться ы'))))
                          ],
                        ))
                  ],
                ))));
  }
}
import 'package:cloud_firestore/cloud_firestore.dart';

class UserSettings{
  static String UID;
  static DocumentSnapshot userDocument = null;
  static String appVersion = 'beta_0_1';
  static String phone;
  static String userLocale = 'eng';
  static void setNewNotificationSettings(bool notificationsOfAcceptance, bool notificationsOfResponding){
    if(userDocument != null) {
      userDocument.data['notifications_of_acceptance'] =
          notificationsOfAcceptance;
      userDocument.data['notifications_of_responding'] =
          notificationsOfResponding;
    }
  }

  static void clearAll(){
    UID = null;
    userDocument = null;
  }
}
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'MainScreens/AddOrderView.dart';
import 'DataBaseAccess/FBManager.dart';
import 'DataBaseAccess/NotifManager.dart';
import 'Entry/UsersOnboarding.dart';
import 'MainScreens/AddOrderView.dart';
import 'MainScreens/AddOrderView.dart';
import 'MainScreens/AddOrderView.dart';
import 'MainScreens/AddOrderView.dart';
import 'MainScreens/LogsView.dart';
import 'MainScreens/OrdersView.dart';
import 'MainScreens/PaymentView.dart';
import 'MainScreens/ProfileView.dart';
import 'Settings/userSettings.dart';

final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
String uid = UserSettings.UID;
bool checked = true;
String deviceToken;

getTok() async{
  FirebaseUser user = await FirebaseAuth.instance.currentUser();
  String token = (await user.getIdToken()).token;
  String  url = "https://us-central1-cleaner-1d1ca.cloudfunctions.net/app/robokassa/payment-form?token=${token}";
  print(url);
  return url.toString();
}

Future<void> switchUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('uid');
  String result = (await FirebaseDatabase.instance.reference().child('executors/' + token + '/number').once()).value;
  print(result);
  return result;
}

_incrementCounterCheck() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('uid');
}

_incrementCounterId() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString('token');
}

_incrementCounterUser() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  UserSettings.UID = prefs.getString('uid');
  return prefs.getString('role');
}

void main() {

  WidgetsFlutterBinding.ensureInitialized();

  //FBManager.GetAppVersions().then((List list){

      _incrementCounterCheck().then((dynamic res) {
        if (res != null) {
          Logs.addNode("Main", "void main", "Building");
          runApp(new MyApp());
        } else {
          runApp(new OnBoarding());
        }
      });
      }



class VersionWrong extends StatefulWidget {

  @override
  _VersionWrongState createState() => _VersionWrongState();
}

class _VersionWrongState extends State<VersionWrong> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Center(
        child: Text("Обновите приложение"),
      ),
    );
  }

}


/// Этот виджет - главный виджет
class MyApp extends StatelessWidget {
  static bool hasChanges = false;
  @override
  Widget build(BuildContext context) {
    Logs.addNode("MyApp", "build", "Building");
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.black,
        statusBarColor: Colors.transparent,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark));

    FBManager.init();
    NotifManager.init();
    print("Main: " + "managers initiated");
     return MaterialApp(
       theme: ThemeData.dark(),
         darkTheme: ThemeData(
           brightness: Brightness.dark,
         ),
         debugShowCheckedModeBanner: false,
         home: MyStatefulWidget());

  }
}


class MyStatefulWidget extends StatefulWidget {

  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 1;

  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static  List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    DashboardPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  printToken(){
    if(deviceToken == null) {
      firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: false),
      );
      firebaseMessaging.getToken().then((token) {
        deviceToken = token;
        print("Main: " + "Instance ID: " + token);
      });
    }
  }

  Future<void> getAvailableOrders() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    print("Main: uid: " + user.uid ?? "ERR");
    DocumentSnapshot snap = await FBManager.getUserStats(user.uid);
    setState(() {
      UserSettings.userDocument = snap;
      UserSettings.UID = snap['token'];
      print("Main: " + "available balance: " + (UserSettings.userDocument['balance'] ?? 'ERR').toString());
      MyApp.hasChanges = false;
    });
  }

  getAvOrders(){
    if(UserSettings.userDocument == null)
      return 0;
    //return UserSettings.userDocument['balance'];
    return 101;
  }

  launchTooltip(){
    //launch('https://www.xvideos.com/?k=Brazzers+gay');
    return 'This feature is not available yet';
  }

  void _showDialog(String orderId, String messageClass) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0)), //this right here
            child: Container(
              height: 170,
              child: Padding(
                  padding: const EdgeInsets.fromLTRB(12.0, 15.0, 12.0, 17.0),
                  child: Column(
                    children: <Widget>[
                      Center(child:Container(
                          padding: EdgeInsets.fromLTRB(2, 7, 2, 12),
                          child: Text(
                            'Радиус поиска вакансий',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ), textAlign: TextAlign.center,
                          ))),
                      Wrap(
                        spacing: 5.0, // horizontal gap between chips
                        runSpacing: 2.0, // gap between row
                        children: <Widget>[

                        ],
                      ),
                    ],
                  )),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    if (Logs.needToShowDialog)
      _showDialog(Logs.orderId, Logs.messageClass);
    Logs.addNode("MyStatefullWidget", "build", "Building");
    printToken();
    if(UserSettings.userDocument == null || MyApp.hasChanges)
      getAvailableOrders();
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text('My tasks'),
          ),

          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text('Profile'),
          ),
        ],
        currentIndex: _selectedIndex,

        selectedItemColor: Colors.red,
        onTap: _onItemTapped,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new FloatingActionButton(
        onPressed:(){
          print("Main: floating button pressed"); },
        tooltip: launchTooltip(),
        backgroundColor: Colors.red,
            child: IconButton(
                icon: Icon(Icons.add),
                onPressed: () async {
                if(getAvOrders() < 100) {
                  String url = await getTok();
                  Logs.log += "\n URL: " + url + "\n";
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => GetCurrentURLWebView(url)));
                }else {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => AddPostStep()));
                }
            }),
      ),
    );
  }

}


import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:virusaround/Settings/userSettings.dart';
import 'package:virusaround/StaticEntities/LocaleStrings.dart';

import '../DataBaseAccess/FBManager.dart';
import '../DataBaseAccess/NotifManager.dart';
import 'ChatRoom.dart';
import 'LogsView.dart';

String uid;
Stream stream;

Color setOrdersColor(int level) {
  switch (level) {
    case 0:
      return Colors.grey;
      break;
    case 1:
      return Colors.white;
      break;
    case 2:
      return Colors.white54;
      break;
  }
}

Container switchWorkType(DocumentSnapshot doc, context) {
  if (doc['work_type'] == "Permanency")
    return Container(
        child: ListTile(
          leading: const Icon(
            Icons.access_time,
            color: Colors.red,
          ),
          title: Text('Schedule'),
          subtitle: RichText(
            text: new TextSpan(
              style: new TextStyle(
                fontSize: 16.0,
                color: Colors.black,
              ),
              children: <TextSpan>[
                new TextSpan(
                  text: 'Type: ',
                  style: Theme.of(context).textTheme.caption,
                ),
                new TextSpan(
                    text: (doc['work_shedule'] ?? Timestamp.now())
                        .toDate()
                        .toString()
                        .substring(0, 16),
                    style: Theme.of(context).textTheme.caption),
                new TextSpan(
                    text: '\nОплата: ', style: Theme.of(context).textTheme.caption),
                new TextSpan(
                    text: (doc['pay_type'] ?? Timestamp.now())
                        .toDate()
                        .toString()
                        .substring(0, 16),
                    style: Theme.of(context).textTheme.caption),
              ],
            ),
          ),
        ));
  else
    return Container(
        child: ListTile(
          leading: const Icon(
            Icons.access_time,
            color: Colors.red,
          ),
          title: Text('Time:'),
          subtitle: RichText(
            text: new TextSpan(
              style: new TextStyle(
                fontSize: 16.0,
                color: Colors.black,
              ),
              children: <TextSpan>[
                new TextSpan(
                  text: 'Start: ',
                  style: Theme.of(context).textTheme.caption,
                ),
                new TextSpan(
                    text: (doc['start_time'] ?? Timestamp.now())
                        .toDate()
                        .toString()
                        .substring(0, 16),
                    style: Theme.of(context).textTheme.caption),
                new TextSpan(
                    text: '\End: ',
                    style: Theme.of(context).textTheme.caption),
                new TextSpan(
                    text: (doc['end_time'] ?? Timestamp.now())
                        .toDate()
                        .toString()
                        .substring(0, 16),
                    style: Theme.of(context).textTheme.caption),
              ],
            ),
          ),
        ));
}

//Container setAdditionalField(String workType){
//  if (workType == null)
//    return Container(child: Text("Ошибка"),);
//  else if (workType == "Постоянная")
//}
//
String tagsToString(List<dynamic> tags) {
//  Logs.log += "\ntags: " + tags.toString() + "\n";
  if (tags != null) {
    String result = "";
    tags.map((dynamic tag) {
      result += tag.toString() + ", ";
    });
    return result.substring(0, result.length - 2);
  } else {
    return "Errpr";
  }
}

String concatMinMax(int min, int max) {
  if (min != null && max != null) if (min == max)
    return min.toString() + " \$";
  else
    return min.toString() + " \$ - " + max.toString() + " \$";
  return "Error";
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Firestore _database = Firestore.instance;
  String nodeName = "tasks";
  ListView listView;

  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
//    FBManager.init();
    if (UserSettings.userDocument != null) {
      stream = FBManager.getEmplsOrders(UserSettings.userDocument['token']);
    } else {
      FirebaseAuth.instance.currentUser().then((user) {
        print("Home: user: " + user.toString());
        uid = user.uid;
        stream = FBManager.getEmplsOrders(uid);
      });
    }
    return Scaffold(
      body: Center(
        child: Container(
            padding: const EdgeInsets.all(10.0),
            child: StreamBuilder<QuerySnapshot>(
              stream: stream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                //print("Home: " + "Home: " + snapshot.data.documents.asMap().isEmpty.toString());
                if (snapshot.data == null) {
                  return skeleton;
                }
                if (snapshot.hasError)
                  return new Text('Error: ${snapshot.error}');
                if (snapshot.data.documents.isEmpty) {
                  return RichText(
                    text: TextSpan(
                      style: Theme.of(context).textTheme.body1,
                      children: [
                        TextSpan(
                          text: 'Post your task' + "\n" + "\n",
                          style: TextStyle(
                            color: Colors.grey,
                          ),
                        ),
                        TextSpan(
                            text: 'Push "',
                            style: TextStyle(color: Colors.grey)),
                        WidgetSpan(
                          child: Padding(
                            padding:
                            const EdgeInsets.symmetric(horizontal: 1.0),
                            child: Icon(
                              Icons.add,
                              size: 18.0,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                        TextSpan(
                            text: '"', style: TextStyle(color: Colors.grey))
                      ],
                    ),
                    textAlign: TextAlign.center,
                  );
                }

                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return new Text('Loading...');
                  default:
                    return ListView(
                      children: snapshot.data.documents
                          .map((DocumentSnapshot document) {
                        return new CustomCard(
                          document: document,
                        );
                      }).toList(),
                    );
                }
              },
            )),
      ),
    );
  }
}

String format_output(String temp, int max_length) {
  if (temp.length <= max_length) {
    return temp;
  } else {
    return temp.substring(0, max_length).trim() + "...";
  }
}

String formatNumberOfViews(int number_of_views)
{
  //number_of_views = 1999; //тест

  if (number_of_views == null)
  {
    return "0";
  }

  String answer = number_of_views.toString();
  if (1000 <= number_of_views && number_of_views <= 999999)
  {
    answer = (number_of_views / 1000).toString() + "K";
  }
  else if (1000000 <= number_of_views && number_of_views <= 9999999)
  {
    answer = (number_of_views / 100).toString().substring(0, 2) + "M";
  }
  return answer;
}

class CustomCard extends StatelessWidget {
  CustomCard({@required this.document});

  final DocumentSnapshot document;

  @override
  Widget build(BuildContext context) {
    return Container(
      //padding: const EdgeInsets.fromLTRB(0, 0, 0, 30),
      padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
      child: Card(
          child: Stack(
            overflow: Overflow.visible,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                //olor: Colors.amber,
                child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => TaskPage(
                                document: document,
                              )));
                    },
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(height: 15),

                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 2.0),
                          child: Text(
                            format_output(document['title'] ?? 'Error',  16).replaceAll("\n\n", "\n"),
                            style:
                            TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
                          ),
                        ),
                        // Text(distOrDesc()),

                        Row(
                          children: <Widget>[

                            Container(
                              padding: const EdgeInsets.fromLTRB(20, 1, 0, 11),
                              child: Text(
                                concatMinMax(document['min_price'], document['max_price']) ,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(20, 1, 0, 11),
                              child: Text(
                                document['date'] ?? "Loading",
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),

                        Container(
                          padding: const EdgeInsets.fromLTRB(20, 0, 0, 15),
                          child: Text(
                              format_output(document['description'] ?? 'Error', 64)
                                  .replaceAll("\n\n", "\n"),
                              style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14.0,
                                  color: Colors.grey)),
                        ),

                        new Container(
                          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                          height: 1,
                          color: Colors.black12,
                          margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: <Widget>[
                            Container(
                                padding: const EdgeInsets.fromLTRB(20, 5, 0, 0),
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      WidgetSpan(
                                        child: Icon(Icons.remove_red_eye, size: 17),
                                      ),
                                      TextSpan(
                                        text: (" " + formatNumberOfViews(document['number_of_views'])),
                                      ),
                                    ],
                                  ),
                                )
                            ),
                            //SizedBox(width: eyePosition),
                            SizedBox(width: 15),
                            Container(
                              padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                              child: Text(formatAddressViewCard(
                                  document['address']
                              ) ?? 'Error',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10.0,
                                      color: Colors.white)),
                            ),


                          ],
                        ),

                        SizedBox(height: 15),
                      ],
                    )),
              ),
//              Positioned(
//                child: FloatingActionButton(
//                  heroTag: document.documentID,
//                  child: Icon(Icons.chat),
//                  onPressed: () {
//                    print(document.documentID);
//                    print(document["token"]);
//                    Navigator.push(context, MaterialPageRoute(builder: (context) => OrderChatView(orderDocument: document.documentID, orderToken : document.data["token"])));
//                    print('FAB tapped!');
//                  },
//                  backgroundColor: Colors.red,
//                ),
//                right: -260,
//                left: 0,
//                bottom: -30,
//              ),
            ],
          )),
    );
  }
}

double eyePosition = 0;

//Вытаскивание имя города
String getCityName(String address) {
  int i = 0;
  while (address[i] != ",")
  {
    i++;
  }
  return address.substring(0, i);
}

// Форматирование вывода адреса
String formatAddressViewCard(String address)
{
  //address = "sdfdsfsdfsd,";
  address = getCityName(address);
  if (address.length >= 30)
  {
    address = address.substring(0, 30) + "...";
  }

  eyePosition = (6*(30 - address.length) + 60).toDouble();
  print(eyePosition);
  return address;
}

class TaskPage extends StatelessWidget {
  TaskPage({@required this.document});

  void _showDialog(context, _document) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0)), //this right here
            child: Container(
              height: 170,
              child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Deactivate this task?',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Column(
                        children: <Widget>[
                          deleteConfirm(_document, context),

                        ],
                      ),
                    ],
                  )),
            ),
          );
        });
  }

  final DocumentSnapshot document;

  String format_output(String temp, int max_length) {
    if (temp.length <= max_length) {
      return temp;
    } else {
      return temp.substring(0, max_length).trim() + "...";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      floatingActionButton: FloatingActionButton.extended(
//        onPressed: () {
//          Navigator.push(
//              context,
//              MaterialPageRoute(
//                  builder: (context) => Workers(
//                        document: document,
//                      )));
//        },
//        icon: Icon(Icons.supervisor_account, color: Colors.white,),
//        label: Text(LOCALES[UserSettings.userLocale]['responders']  ?? 'Error' , style: TextStyle(color: Colors.white),),
//        backgroundColor: Colors.red,
//      ),
      appBar: AppBar(
        title: Container(
            child: Text(
              format_output(document['title']  ?? 'Error' , 10),
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 24.0, color: Colors.white),
            )),
        leading: BackButton(color: Colors.white),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: <Widget>[
          FlatButton(
            onPressed: () => {DeleteDialog(context, document)},
            color: Colors.transparent,
            padding: EdgeInsets.all(5.0),
            child: Column(
              // Replace with a Row for horizontal icon + text
              children: <Widget>[
                Icon(
                  Icons.archive,
                  color: Colors.grey,
                ),
                Text(
                  LOCALES[UserSettings.userLocale]['toArchive'] ?? 'Error',
                  style: TextStyle(color: Colors.grey),
                )
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(

        child: Column(children: <Widget>[
//                  new TextSpan(text: concatMinMax(document['min_price'], document['max_price']) + " / " + document['pay_type'] ?? "",
          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.attach_money,
                color: Colors.red,
              ),
              title: Text(LOCALES[UserSettings.userLocale]['payment'] ?? 'Error', textScaleFactor: 1.2),
              subtitle: Text(
                concatMinMax(document['min_price'], document['max_price']),

                textScaleFactor: 1.2,
              ),
            ),
          ),

          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.work,
                color: Colors.red,
              ),
              title: Text(LOCALES[UserSettings.userLocale]['type_of_place'] ?? 'Error'),
              subtitle: Text(document['work_type'] ??
                  " " + " " + document['work_shedule'] ??
                  ""),
            ),
          ),

          document['start_time'] != null
              ? Container(
              child: ListTile(
                leading: const Icon(
                  Icons.access_time,
                  color: Colors.red,
                ),
                title: Text(LOCALES[UserSettings.userLocale]['time'] ?? 'Error'),
                subtitle: RichText(
                  text: new TextSpan(
                    style: new TextStyle(
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                        text: LOCALES[UserSettings.userLocale]['begin'] ?? 'Error',
                        style: Theme.of(context).textTheme.caption,
                      ),
                      new TextSpan(
                          text: (document['start_time'] ?? Timestamp.now())
                              .toDate()
                              .toString()
                              .substring(0, 16),
                          style: Theme.of(context).textTheme.caption),
                      new TextSpan(
                          text: LOCALES[UserSettings.userLocale]['end'] ?? 'Error',
                          style: Theme.of(context).textTheme.caption),
                      new TextSpan(
                          text: (document['end_time'] ?? Timestamp.now())
                              .toDate()
                              .toString()
                              .substring(0, 16),
                          style: Theme.of(context).textTheme.caption),
                    ],
                  ),
                ),
              ))
              : ListTile(
              leading: const Icon(
                Icons.access_time,
                color: Colors.red,
              ),
              title: Text(LOCALES[UserSettings.userLocale]['time'] ?? 'Error'),
              subtitle: Text(LOCALES[UserSettings.userLocale]['time_table'] ?? 'Error' + document['schedule_type'] ?? ' Error')),
          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.phone,
                color: Colors.red,
              ),
              title: Text(LOCALES[UserSettings.userLocale]['contacts'] ?? 'Error'),
              subtitle: Text(document['phone_number'] ?? 'Error'),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.location_on,
                color: Colors.red,
              ),
              title: Text(LOCALES[UserSettings.userLocale]['address'] ?? 'Error'),
              subtitle: Text(document['address'] ?? 'Error'),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              launch(
                  'https://www.google.com/maps/search/?api=1&query=${document['geopos']['geopoint'].latitude.toString()},${document['geopos']['geopoint'].longitude.toString()}');
            },
            child: Image.network(
              'https://static-maps.yandex.ru/1.x/?l=map&pt=${document['geopos']['geopoint'].longitude.toString()},${document['geopos']['geopoint'].latitude.toString()},pm2dgl,l&z=15&&size=600,300',
              width: 320,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.description,
                color: Colors.red,
              ),
              title: Text(LOCALES[UserSettings.userLocale]['description'] ?? 'Error'),
              subtitle: Text(document['description'] ?? 'Error'),
            ),
          ),
          SizedBox(
            height: 80,
          ),
        ]),
      ),
    );
  }
}

_launchTexter(String text) async {
  String url = LOCALES[UserSettings.userLocale]['sms'] + text;
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class Workers extends StatelessWidget {
  Workers({@required this.document});

  Future<dynamic> _getSnap(List list) async {
    print("Home: " + "len: " + listOfMaps.length.toString() ?? 'PROBLEM2');
    for (int i = 0; i < listOfMaps.length; i++)
      print("Uid at :" + i.toString() + " is: " + listOfMaps[i]['token']);
    listOfMaps = [];

    dynamic snapshot = await FBManager.getUserStatsList(list);
    print("Home: " + "snap is " + snapshot.toString());
    print(
        "Home: " + "is not empty: " + snapshot.documents.isNotEmpty.toString());
    if (snapshot.documents.isNotEmpty) {
      snapshot.documents.forEach((DocumentSnapshot document) {
        //print("Home: " + "doc: " + document.data.toString());
        listOfMaps.add(document.data);
        //print("Home: " + 'doc2' + listOfMaps.toString());
        initColorsList();
        return listOfMaps;
      });
    }
    initColorsList();
    return listOfMaps;
  }

  Color setUserColor(int index) {
    if (document['applied'].contains(listOfMaps[index]['token'])) {
      listOfColors[index] = Colors.red;
      listOfText[index] = Colors.white;
      help[index] = "Этот рабочий подтвердил, что придет!";
      print("SetUserColor: " + document['applied'].toString() + " to: Green");
    } else if (appliedWorkers.contains(listOfMaps[index]['token'])) {
      listOfColors[index] = Colors.white54;
      listOfText[index] = Colors.black;
      help[index] = "HELP1";
      print("SetUserColor: " + appliedWorkers.toString() + " to: White54");
    } else {
      listOfColors[index] = Colors.white;
      listOfText[index] = Colors.black;
      help[index] = "Проведите в сторону в сторону для действия";
      print("SetUserColor: " + listOfMaps[index]['token'] + " to: White");
    }
  }

  String _formatPhoneNum(String number){
    if (number.length > 10){
      return number.substring(0, 2) + " "+ number.substring(2, 5) + " " + number.substring(5, 8) + "-" + number.substring(8, 10) + "-" + number.substring(10,12);

    }
    return number;
  }
  _launchCaller(String text) async {
    String url = "tel:" + text;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static List listOfMaps = new List();
  final DocumentSnapshot document;
  List<String> appliedDevices = new List();
  List<String> declinedWorkers = new List();
  List<String> appliedWorkers = new List();

  delegateConfirmation() {
    Logs.log += "\n\ndevices: " +
        appliedDevices.toString() +
        "\n\ndeclined:" +
        declinedWorkers.toString() +
        "\n\napplied: " +
        appliedWorkers.toString();
    if (declinedWorkers.isNotEmpty) {
      FBManager.removeFromOrder(declinedWorkers, document);
    }
    if (appliedDevices.isNotEmpty) {
      FBManager.pushToQuery(appliedWorkers, document);
      NotifManager.NotifyWorkersOfConfirmation(
          appliedDevices, document.documentID, document.data['description'], document['title'] ?? "ERR");
    }
  }

  var defaultColor = Colors.white;

  List<Color> listOfColors;
  List<Color> listOfText;
  List<String> help;

  initColorsList() {
    listOfColors = List(listOfMaps.length);
    listOfText = List(listOfMaps.length);
    help = List(listOfMaps.length);
    print(
        "list of corors inited: " + listOfColors.length.toString() ?? 'ERROR');
    for (int i = 0; i < listOfMaps.length; i++) {
      print("initing color at: " + i.toString());
      setUserColor(i);
    }
  }

  @override
  Widget build(BuildContext context) {
    print("initColorsList string: " + listOfColors.toString());
  }
}

//        title: Text('${document['workers'][index]}'),
Widget skeleton = ListView.builder(
    scrollDirection: Axis.vertical,
    physics: BouncingScrollPhysics(),
    itemCount: 4,
    itemBuilder: (BuildContext context, int index) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Colors.white70),
          child: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SkeletonAnimation(
                  child: Container(
                    width: 70.0,
                    height: 70.0,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, bottom: 5.0),
                      child: SkeletonAnimation(
                        child: Container(
                          height: 15,
                          width: MediaQuery.of(context).size.width * 0.6,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey[300]),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: SkeletonAnimation(
                          child: Container(
                            width: 60,
                            height: 13,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.grey[300]),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    });


Widget deleteConfirm(DocumentSnapshot documentSnapshot, context) {
  return Container(
    child: OutlineButton(
      color: Colors.grey,
      onPressed: () {
        Logs.log += "\nDEACTIVATING" + documentSnapshot.documentID + "\n";
        FBManager.deactivateOrder(documentSnapshot);
        Navigator.of(context).pop();
      },
      child: Text('Archive', style: TextStyle(color: Colors.black)),
    ),
  );
}

Future<bool> DeleteDialog(BuildContext context, document) async {
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16.0))),
          title: Text('Deactivate this task?'),
          actions: <Widget>[
            new FlatButton(
                onPressed: () {
                  FBManager.deactivateOrder(document);
                  Navigator.of(context, rootNavigator: true).pop();
                  Navigator.of(context, rootNavigator: true).pop();
                },
                child: Text(
                  "YES",
                  style: TextStyle(color: Colors.grey),
                )),
            new FlatButton(
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                },
                child: Text(
                  "NO",
                  style: TextStyle(color: Colors.red),
                ))
          ],
        );
      });
}

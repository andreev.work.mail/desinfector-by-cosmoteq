
final ENG = {
      'responders' : "Responded",
      'payment':"Payment",
      'address':"Address",
      'acceptWorker' : "Responded componies",
      'toArchive':"Delete",
      'type_of_work':"Тип работы",
      'options':"Options",
      'begin':"Start",
      'time':"Time",
      'end':"\nEnd: ",
      'time_table':"Schedule",
      'time':"Schedule",
      'description':"Description",
      'sms':"sms:",
      'home':"",
      'work_schedule':"Test",
      'contacts':"Contact phone number",
      'type_of_place':"Place to sanitize"
};

final RUS = {'responders' : "Откликнувшиеся",
      'acceptWorker' : "Откликнувшиеся работники",
      'toArchive':"Archive",
      'type_of_work':"Тип работы",
      'options':"Критерии",
      'begin':"Начало",
      'time':"Время",
      'end':"\nОкончание: ",
      'time_table':"График работы",
      'time':"График",
      'contacs':"Контакты",
      'description':"Описание",
      'sms':"sms:",
      'home':"",
};

final UKR = {'responders' : "Відгукнулися",
      'acceptWorker' : "Відгукнулися працівники"};

final LOCALES = {'rus' : RUS, 'ukr': UKR, 'eng':ENG};


import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:skeleton_text/skeleton_text.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:virusaround/Settings/userSettings.dart';

import '../DataBaseAccess/FBManager.dart';
import 'LogsView.dart';

String uid;
Stream stream;

Color setOrdersColor(int level) {
  switch (level) {
    case 0:
      return Colors.grey;
      break;
    case 1:
      return Colors.white;
      break;
    case 2:
      return Colors.white54;
      break;
  }
}

//Container setAdditionalField(String workType){
//  if (workType == null)
//    return Container(child: Text("Ошибка"),);
//  else if (workType == "Постоянная")
//}
//
String tagsToString(List<dynamic> tags){
//  Logs.log += "\ntags: " + tags.toString() + "\n";
  if(tags != null){
    String result = "";
    tags.map((dynamic tag){result += tag.toString() + ", ";});
    return result.substring(0, result.length - 2);
  }else{
    return "Error";
  }
}

String concatMinMax(int min, int max){
  if (min != null && max != null)
    if (min == max)
      return min.toString() + " \$";
    else
      return min.toString() + " \$ - " + max.toString() + " \$";
  return "Error";
}

class DeactivatedOrders extends StatefulWidget {
  @override
  _DeactivatedPageState createState() => _DeactivatedPageState();
}


String format_output(String temp, int max_length)
{
  if (temp.length <= max_length)
  {
    return temp;
  }
  else
  {
    return temp.substring(0, max_length).trim() + "...";
  }
}

class _DeactivatedPageState extends State<DeactivatedOrders> {
  Firestore _database = Firestore.instance;
  String nodeName = "tasks";
  ListView listView;

  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    FBManager.init();
    if (UserSettings.userDocument != null) {
      stream =
          FBManager.getDeactivatedOrders(UserSettings.userDocument['token']);
    } else if (UserSettings.UID != null) {
      stream = FBManager.getDeactivatedOrders(UserSettings.UID);
    } else {
      FirebaseAuth.instance.currentUser().then((user) {
        print("Home: user: " + user.toString());
        uid = user.uid;
        stream = FBManager.getDeactivatedOrders(uid);
      });
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Desinfection history'),
        backgroundColor: Colors.red,
      ),
      body: Center(
        child: Container(
            padding: const EdgeInsets.all(10.0),
            child: StreamBuilder<QuerySnapshot>(
              stream: stream,
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                //print("Home: " + "Home: " + snapshot.data.documents.asMap().isEmpty.toString());
                if (snapshot.data == null){
                  return skeleton;
                }
                if (snapshot.hasError)
                  return new Text('Error: ${snapshot.error}');
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                    return new Text('Loading...');
                  default:
                    return ListView(
                      children: snapshot.data.documents
                          .map((DocumentSnapshot document) {
                        return new CustomCard(
                          document: document,
                        );
                      }).toList(),
                    );
                }
              },
            )),
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  CustomCard({@required this.document});

  final document;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TaskPage(
                        document: document,
                      )));
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: 15),

                Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 2.0),
                  child: Text(
                    format_output(document['title'] ?? 'Write-off error', 16).replaceAll("\n\n", "\n"),
                    style:
                    TextStyle(fontWeight: FontWeight.bold, fontSize: 24.0),
                  ),
                ),
                // Text(distOrDesc()),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 1, 0, 11),
                  child: Text(
                    concatMinMax(document['min_price'], document['max_price']) + " / " + document['pay_type'] ?? "",
                    style: TextStyle(fontSize: 14.0, color: Colors.red, fontWeight: FontWeight.bold),
                  ),
                ),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 0, 0, 15),
                  child: Text(
                      format_output(document['body'] ?? 'Write-off error', 64).replaceAll("\n\n", "\n"),
                      style: TextStyle(fontWeight: FontWeight.w400, fontSize: 14.0, color: Colors.black87)
                  ),
                ),

                new Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
                  margin: const EdgeInsets.only(left: 20.0, right: 20.0),),

                Container(
                  padding: const EdgeInsets.fromLTRB(20, 5, 0, 0),

                  child: Text(
                      document['address'] ?? 'Write-off error',
                      style: TextStyle(fontWeight: FontWeight.w500, fontSize: 10.0, color: Colors.black26)
                  ),
                ),



                SizedBox(height: 15),
              ],
            )));;
  }
}

class TaskPage extends StatelessWidget {
  TaskPage({@required this.document});
  final document;


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Container(
            child: Text(
                format_output(document['title'] ?? 'Ошибка заголовка', 16),
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 24.0, color: Colors.black),
            )),
        leading: BackButton(color: Colors.black),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        actions: <Widget>[
        ],
      ),
      body: SingleChildScrollView(
        child: Column(children: <Widget>[
//                  new TextSpan(text: concatMinMax(document['min_price'], document['max_price']) + " / " + document['pay_type'] ?? "",
          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.attach_money,
                color: Colors.red,
              ),
              title: Text("Payment"),
              subtitle: Text(
                  concatMinMax(document['min_price'], document['max_price']) +
                      " / " +
                      document['pay_type'] ??
                      ""),
            ),
          ),

          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.work,
                color: Colors.red,
              ),
              title: Text(''),
              subtitle: Text(document['work_type'] ??
                  " " + " " + document['work_shedule'] ??
                  ""),
            ),
          ),
          Container(

            child: new ListTile(
              leading: const Icon(
                Icons.local_offer,
                color: Colors.red,
              ),
              title: Text('Критерии'),
              subtitle: Text(document['tags'].toString().substring(1,document['tags'].toString().length - 1) ?? "Нет"),
            ),
          ),

          document['start_time'] != null
              ? Container(
              child: ListTile(
                leading: const Icon(
                  Icons.access_time,
                  color: Colors.red,
                ),
                title: Text('Время'),
                subtitle: RichText(
                  text: new TextSpan(
                    style: new TextStyle(
                      fontSize: 16.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(text: 'Начало: ' ,style: Theme.of(context).textTheme.caption,
                      ),
                      new TextSpan(
                          text: (document['start_time'] ?? Timestamp.now())
                              .toDate().toString()
                              .substring(0, 16),style: Theme.of(context).textTheme.caption),
                      new TextSpan(
                          text: '\nОкончание: ',style: Theme.of(context).textTheme.caption),
                      new TextSpan(text: (document['end_time'] ?? Timestamp.now())
                          .toDate().toString()
                          .substring(0, 16),style: Theme.of(context).textTheme.caption),
                    ],
                  ),
                ),
              )
          )
              : ListTile(
              leading: const Icon(
                Icons.access_time,
                color: Colors.red,
              ),
              title: Text('График'),
              subtitle: Text("График работы: " + document['work_shedule'])),


          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.location_on,
                color: Colors.red,
              ),
              title: Text('Контакты'),
              subtitle: Text(document['address'] ?? ""),
            ),
          ),
          SizedBox(height: 20,),
          InkWell(
            onTap: () {
              launch(
                  'https://www.google.com/maps/search/?api=1&query=${document['geopos']['geopoint'].latitude.toString()},${document['geopos']['geopoint'].longitude.toString()}');
            },
            child: Image.network(
              'https://static-maps.yandex.ru/1.x/?l=map&pt=${document['geopos']['geopoint'].longitude.toString()},${document['geopos']['geopoint'].latitude.toString()},pm2dgl,l&z=15&&size=600,300',
              width: 320,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            child: new ListTile(
              leading: const Icon(
                Icons.description,
                color: Colors.red,
              ),
              title: Text('Описание'),
              subtitle: Text(document['body'] ?? ""),
            ),
          ),
          SizedBox(height: 80,),

        ]),
      ),
    );
  }
}

Widget deleteConfirm(DocumentSnapshot documentSnapshot) {
  return Container(
    child: OutlineButton(
      color: Colors.grey,
      onPressed: () {
        Logs.log += "\nDEACTIVATING" + documentSnapshot.documentID + "\n";
        FBManager.deactivateOrder(documentSnapshot);
      },
      child: Text('Деактивировать', style: TextStyle(color: Colors.black)),
    ),
  );
}

Widget cancel(DocumentSnapshot documentSnapshot) {
  return Container(
    child: OutlineButton(
      color: Colors.grey,
      onPressed: () {
        Logs.log += "\nDEACTIVATING" + documentSnapshot.documentID + "\n";
        FBManager.deactivateOrder(documentSnapshot);
      },
      child: Text('Деактивировать', style: TextStyle(color: Colors.black)),
    ),
  );
}

//        title: Text('${document['workers'][index]}'),
Widget skeleton = ListView.builder(
    scrollDirection: Axis.vertical,
    physics: BouncingScrollPhysics(),
    itemCount: 4,
    itemBuilder: (BuildContext context, int index) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Colors.white70),
          child: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SkeletonAnimation(
                  child: Container(
                    width: 70.0,
                    height: 70.0,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0, bottom: 5.0),
                      child: SkeletonAnimation(
                        child: Container(
                          height: 15,
                          width: MediaQuery.of(context).size.width * 0.6,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.grey[300]),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Padding(
                        padding: const EdgeInsets.only(right: 5.0),
                        child: SkeletonAnimation(
                          child: Container(
                            width: 60,
                            height: 13,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: Colors.grey[300]),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
    );

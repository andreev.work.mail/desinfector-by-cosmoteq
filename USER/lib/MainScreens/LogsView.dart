import 'package:flutter/material.dart';


class Logs extends StatelessWidget {
  static String log = "/logs started/\n";
  static addNode(String className, String methodName, String info){
    log += "\n---" + className + "---\n" + "===" + methodName + "===\n" + info + "\n---//---\n";
  }
  static bool needToShowDialog = false;
  static String orderId;
  static String messageClass;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Expanded(
            child: new SingleChildScrollView(
              child:
              Center (
                child :Text(
                log ?? 'empty',
                style: new TextStyle(
                  fontSize: 16.0, color: Colors.black45,
                ),
              ),
            ),
            ),
          ),
          new FlatButton(onPressed: (){Navigator.of(context).pop();}, child: Text('Назад')),
        ],
      ),
    );
  }
}

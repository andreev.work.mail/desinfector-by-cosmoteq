import 'dart:math';


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:virusaround/MainScreens/LogsView.dart';
import 'package:virusaround/Settings/userSettings.dart';
import 'package:virusaround/StaticEntities/DataBaseNamings.dart';

import '../main.dart';

class FBManager {
  static Firestore fbStore;
  static Geoflutterfire geo;

  static void init() {
    fbStore = Firestore.instance;
    geo = Geoflutterfire();
  }
  
  static checkUserIdIfSet() async {
    if (UserSettings.UID == null)
      UserSettings.UID = (await FirebaseAuth.instance.currentUser()).uid;
  }

  static Future<void> addOrder(Map<String, dynamic> map, DocumentSnapshot _documentSnapshot) async { /// бля лучше вот эту херню не трогать
    try {
      await checkUserIdIfSet();
      fbStore
          .collection(ORDERS_COLLECTION)
          .add(map);
      fbStore
          .collection(USERS_COLLECTION)
          .document(UserSettings.UID)
          .updateData({'balance' : _documentSnapshot.data['balance'] - 249});
      MyApp.hasChanges = true;
    }
    catch (e) {
      Logs.addNode("FBManager", "AddOrder", e.toString());
    }
  }

  static Future<void> editOrder(Map<String, String> map) async {
    try {
      await fbStore
          .collection(ORDERS_COLLECTION)
          .document(map['token'])
          .updateData(map);
    } catch (e) {
      Logs.addNode("FBManager", "EditOrder", e.toString());
    }
  }

  static Future<void> deleteOrder(String token) async {
    try {
      await fbStore
          .collection(ORDERS_COLLECTION)
          .document(token)
          .delete();
    } catch (e) {
      Logs.addNode("FBManager", "Delete", e.toString());
    }
  }

  static Future<void> deactivateOrder(DocumentSnapshot _documentSnapshot) async {
    try {
      await fbStore
          .collection(DEACTIVATED_ORDERS_COLLECTION)
          .add(_documentSnapshot.data);
      await fbStore
          .collection(ORDERS_COLLECTION)
          .document(_documentSnapshot.documentID)
          .delete();
    } catch (e) {
      Logs.addNode("FBManager", "DeactivateOrder", e.toString());
    }
  }

  static Future<DocumentSnapshot> getUser(String _uid) async { /// ПРОВЕРКА ПОЛЬЗОВАТЕЛЯ НА НАЛИЧИЕ В БАЗЕ ДАННЫХ
    try {
      DocumentSnapshot _documentSnapshot = await fbStore
          .collection(USERS_COLLECTION)
          .document(_uid)
          .get();
      return _documentSnapshot;
    } catch (e) {
      Logs.addNode("FBManager", "GetUser", e.toString());
    }
  }

  static Future<void> addNewUser(Map<String, dynamic> map) async {
    try {
      await checkUserIdIfSet();
      await fbStore
          .collection(USERS_COLLECTION)
          .document(UserSettings.UID)
          .setData(map);
    } catch (e) {
      Logs.addNode("FBManager", "AddNewUser", e.toString());
    }
  }

  static Future<void> editUser(Map<String, String> map) async {
    try {
      await checkUserIdIfSet();
      await fbStore
          .collection(USERS_COLLECTION)
          .document(UserSettings.UID)
          .updateData(map);
    } catch (e) {
      Logs.addNode("FBManager", "EditUser", e.toString());
    }
  }

  static Future<void> applyToOrder(String orderToken, String _uid) async { /// ВОРКЕР ПРИНИМЕТ ЗАКАЗ
    try {
      List<String> list = new List();
      list.add(_uid);
      var doc = await fbStore
          .collection(ORDERS_COLLECTION)
          .document(orderToken)
          .get();
      for (String node in doc.data['workers'])
        list.add(node);
      await fbStore
          .collection(ORDERS_COLLECTION)
          .document(orderToken)
          .updateData({'workers': list});
    } catch (e) {
      Logs.addNode("FBManager", "ApplyToOrder", e.toString());
    }
  }

  static Future<DocumentSnapshot> getOrderStats(String orderToken) async {
    /// ДАЕТ ДОСТУП КО ВСЕМ ДАННЫМ ПО ЗАКАЗУ
    /// ДАЛЕЕ ВО ФРОНТЕ НАДО СДЕЛТЬ ФИЛЬТР ПО ТОМУ, КОМУ ЗАКАЗ ПОКАЗЫВАЕТСЯ
    try {
      return await fbStore
          .collection(ORDERS_COLLECTION)
          .document(orderToken)
          .get();
    } catch (e) {
      Logs.addNode("FBManager", "GetOrderStats", e.toString());
    }

    /// ПОСМОТРИ В registration.dart МЕТОД CheckUser, ТАМ ПРИМЕР ЧТО ЭТА ХРЕНЬ ВОЗВРАЩАЕТ
    /// ТОЛЬКО ТУТ ЕЩЕ ТАБЛИЦА ВОРКЕРОВ, КОТОРЫЕ СОГЛАСИЛИСЬ НА РАБОТУ
  }

  static Future<void> cancelOrder(String orderToken, String _uid) async { /// ОТКАЗ ОТ ЗАКАЗА РАБОТНИКОМ
    try {
      List<String> list = new List();
      var doc = await fbStore
          .collection(ORDERS_COLLECTION)
          .document(orderToken)
          .get();
      for (String node in doc.data['workers'])
        list.add(node);
      list.remove(_uid);
      await fbStore
          .collection(ORDERS_COLLECTION)
          .document(orderToken)
          .updateData({'workers': list});
    } catch (e) {
      Logs.addNode("FBManager", "CancelOrder", e.toString());
    }
  }

  static dynamic getOrders(double lat, double lon, double radius, String _uid) { /// ПОЛУЧЕНИЕ ЗАКАЗОВ ДЛЯ РАБОТНИКА ПО ГЕОПОЛОЖЕНИЮ
    try {
      String prevId;
      if (lat != null) {
        return geo
            .collection(collectionRef: fbStore.collection(ORDERS_COLLECTION))
            .within(
            center: geo.point(latitude: lat, longitude: lon),
            radius: radius,
            field: 'geopos')
            .where((List<DocumentSnapshot> list) {
          list.removeWhere((DocumentSnapshot document) {
            if (prevId == document.documentID)
              return true;
            else
              prevId = document.documentID;
            return document.data['workers'].contains(_uid);
          });
          return true;
        });
      }
    } catch (e) {
      Logs.addNode("FBManager", "GetOrders", e.toString());
    }
  }

  static dynamic getAppliedOrders(String _uid) {  /// ПОЛУЧЕНИЕ ЗАКАЗОВ, ЗА КОТОРЫЕ ВЗЯЛСЯ ПОЛЬЗОВАТЕЛЬ
    try {
      return fbStore
          .collection(ORDERS_COLLECTION)
          .where('workers', arrayContains: _uid)
          .snapshots();
    } catch (e) {
      Logs.addNode("FBManager", "GetAppliedOrders", e.toString());
    }
  }

  static dynamic getUserStats(String _uid) async { /// ПОЛУЧЕНИЕ ДОКУМЕНТА ПО UID
    try {
      return await fbStore
          .collection(USERS_COLLECTION)
          .document(_uid)
          .get();
    } catch (e) {
      Logs.addNode("FBManager", "GetUserStats", e.toString());
    }
  }

  static dynamic getEmplsOrders(String _uid){
    try {
      return fbStore
          .collection(ORDERS_COLLECTION)
          .where('token', isEqualTo: _uid)
          .snapshots();
    } catch (e) {
      Logs.addNode("FBManager", "GetEmplsOrders", e.toString());
    }
  }

  static dynamic getDeactivatedOrders(String _uid){
    try {
      return fbStore
          .collection(DEACTIVATED_ORDERS_COLLECTION)
          .where('token', isEqualTo: _uid)
          .snapshots();
    } catch (e) {
      Logs.addNode("FBManager", "GetDeactivatedOrders", e.toString());
    }
  }

  static pushToQuery(List workersIds, DocumentSnapshot _documentSnapshot){
    try {
      List _tempList = List.from(_documentSnapshot.data['query']);
      _tempList.removeWhere((elem) {
        return workersIds.contains(elem);
      });
      _tempList.addAll(workersIds);
      fbStore
      .collection(ORDERS_COLLECTION)
      .document(_documentSnapshot.documentID)
      .updateData({"query" : _tempList});
    } catch (e) {
      Logs.addNode("FBManager", "PushToQuery", e.toString());
    }
  }

  static Future<dynamic> getUserStatsList(List _usersList) async{
    try {
      return await fbStore
          .collection(USERS_COLLECTION)
          .where('token', whereIn: _usersList)
          .getDocuments();
    } catch (e) {
      Logs.addNode("FBManager", "GetUserStatsList", e.toString());
    }
  }

  static removeFromOrder(List workersId, DocumentSnapshot _documentSnapshot){
    try {
      List _tempListWorkers = List.from(_documentSnapshot.data['workers']);
      List _tempListQuery = List.from(_documentSnapshot.data['query']);
      List _tempListApplied = List.from(_documentSnapshot.data['applied']);
      _tempListWorkers.removeWhere((elem) {
        return workersId.contains(elem);
      });
      _tempListWorkers = List.from(_documentSnapshot.data['query']);
      _tempListQuery.removeWhere((elem) {
        return workersId.contains(elem);
      });
      _tempListQuery = List.from(_documentSnapshot.data['applied']);
      _tempListApplied.removeWhere((elem) {
        return workersId.contains(elem);
      });
      fbStore
          .collection(ORDERS_COLLECTION)
          .document(_documentSnapshot.documentID)
          .updateData({
        "workers": _tempListWorkers,
        "query": _tempListQuery,
        "applied": _tempListApplied});
    } catch (e) {
      Logs.addNode("FBManager", "RemoveFormOrder", e.toString());
    }
  }


  static Future<void> updateBalance() async {
    try {
      await checkUserIdIfSet();
      UserSettings.userDocument.data['balance'] = (await fbStore
          .collection(USERS_COLLECTION)
          .document(UserSettings.UID)
          .get())
          .data['balance'];
    } catch (e) {
      Logs.addNode("FBManager", "UpdateBalance", e.toString());
    }
  }

  static Future<void> createChatroom(String targetToken, String orderToken, String userName) async {
    try {
      await fbStore
          .collection(CHATROOMS_COLLECTION)
          .add({
        "company" : UserSettings.userDocument['name'],
        "company_token" : UserSettings.UID,
        "user" : targetToken,
        "user_name" : userName,
        "activation_time" : Timestamp.now(),
        "order_token" : orderToken});
    } catch (e) {
      Logs.addNode("FBManager", "createChatroom", e.toString());
    }
  }

  static Future<void> sendMessage(String chatroomToken, String text) async {
    try {
      await fbStore
          .collection(CHATROOMS_COLLECTION)
          .document(chatroomToken)
          .collection(MESSAGES_SUBCOLLECTION)
          .add({
        "user" : UserSettings.UID,
        "text" : text,
        "post_time" : Timestamp.now(),
        "name" : UserSettings.userDocument['name']});
    } catch (e) {
      Logs.addNode("FBManager", "SendMessage", e.toString());
    }
  }

  static Stream<QuerySnapshot> getChatStream(String chatroomToken) {
    try {
      return fbStore
          .collection(CHATROOMS_COLLECTION)
          .document(chatroomToken)
          .collection(MESSAGES_SUBCOLLECTION)
          .orderBy("post_time", descending: true)
          .snapshots();
    } catch (e) {
      Logs.addNode("FBManager", "getChatStream", e.toString());
    }
  }

  static Future<List<DocumentSnapshot>> getChatroomsByOrderToken(String orderToken) async {
    try {
      return (await fbStore
          .collection(CHATROOMS_COLLECTION)
          .where("order_token", isEqualTo: orderToken)
          .getDocuments())
          .documents;
    } catch (e) {
      Logs.addNode("FBManager", "getChatroomsByOrder", e.toString());
    }
  }

  static Future<DocumentSnapshot> getLastMessage(String chatroomToken) async {
    try {
      return (await fbStore
          .collection(CHATROOMS_COLLECTION)
          .document(chatroomToken)
          .collection(MESSAGES_SUBCOLLECTION)
          .orderBy("post_time", descending: true)
          .limit(1)
          .getDocuments()).documents.elementAt(0);
    } catch (e) {
      Logs.addNode("FBManager", "getLastMessage", e.toString());
    }
  }

  static Future<void> deleteChatroom(String chatroomToken) async {
    try {
      return await fbStore
          .collection(CHATROOMS_COLLECTION)
          .document(chatroomToken)
          .delete();
    } catch (e) {
      Logs.addNode("FBManager", "deleteChatroom", e.toString());
    }
  }

  static Future<void> updateNotificationSettings(bool notificationsOfAcceptance, bool notificationsOfResponding) async {
    try {
      await checkUserIdIfSet();
      await fbStore
          .collection(USERS_COLLECTION)
          .document(UserSettings.UID)
          .updateData({'notifications_of_acceptance': notificationsOfAcceptance,
        'notifications_of_responding': notificationsOfResponding});
    } catch (e) {
      Logs.addNode("FBManager", "UpdateNotifications", e.toString());
    }
  }

}


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:country_list_pick/country_list_pick.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:virusaround/DataBaseAccess/FBManager.dart';
import 'package:virusaround/Settings/userSettings.dart';
import 'package:virusaround/StaticEntities/DataBaseNamings.dart';

import '../main.dart';


class CitiesService {
  static final List<String> cities = [
    'Beirut',
    'Damascus',
    'San Fransisco',
    'Rome',
    'Los Angeles',
    'Madrid',
    'Bali',
    'Barcelona',
    'Paris',
    'Bucharest',
    'New York City',
    'Philadelphia',
    'Sydney',
  ];

  static List<String> getSuggestions(String query) {
    List<String> matches = List();
    matches.addAll(cities);

    matches.retainWhere((s) => s.toLowerCase().contains(query.toLowerCase()));
    return matches;
  }
}

class ProfileSettings extends StatefulWidget {
  @override
  _ProfileSettings createState() => _ProfileSettings();
}

bool notificationsOfResponding = UserSettings.userDocument['notifications_of_responding']; // Работник откликнулся
bool notificationsOfAcceptance = UserSettings.userDocument['notifications_of_acceptance']; // Работник подтвердил, что придет

// Переменны, которые выводятся в полях заполения при заполнении. Обновляются при закполнении ячейки
String _name = UserSettings.userDocument['name'].toString();
String _surname = UserSettings.userDocument['surname'].toString();
String _email = UserSettings.userDocument['email'].toString();
String _country = UserSettings.userDocument['country'].toString();
String _index = UserSettings.userDocument['index'].toString();
String _region = UserSettings.userDocument['region'].toString();
String _phone = UserSettings.userDocument['phone'].toString();



class _ProfileSettings extends  State<ProfileSettings> {
  final TextEditingController _typeAheadController = TextEditingController();
  String _selectedCity;

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
          systemNavigationBarColor: Colors.transparent,
          statusBarColor: Colors.transparent,
          systemNavigationBarIconBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark
        )
    );

    return Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () async {
            return showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext context) {
                  return new AlertDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(16.0))),
                    title: Text('Do you really want to apply settings?', textAlign: TextAlign.center,),
                    content: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children : <Widget>[
                          Container(
                            child: new FlatButton(
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true).pop();
                                },
                                child: Text(
                                  "Cancel",
                                  style: TextStyle(color: Colors.red),
                                )),
                          ),

                          Container(
                            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                            child: new FlatButton(
                              color: Colors.green,
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(5.0),
                                    side: BorderSide(color: Colors.green)
                                ),
                                onPressed: () async {
                                  await FBManager.fbStore
                                      .collection(USERS_COLLECTION)
                                      .document(UserSettings.UID)
                                      .updateData({
                                    'name': _name,
                                    'phone': _phone,
                                    'surname': _surname,
                                    'role' : 'empl',
                                    'balance' : 100,
                                    'notifications_of_responding' : false,
                                    'notifications_of_acceptance' : true,
                                    'email':_email,
                                    'country':_country,
                                    'index':_index,
                                    'region':_region,
                                  });

                                  // Обновление данных document из FB
                                  FirebaseUser user = await FirebaseAuth.instance.currentUser();
                                  UserSettings.userDocument = await FBManager.getUserStats(user.uid);

                                  // Закрытие окна и возврат в профиль
                                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyApp()));
                                },
                                child: Text(
                                  "OK",
                                  style: TextStyle(color: Colors.white),
                                )),
                          ),

                        ],
                    ),
                    actions: <Widget>[



                    ],
                  );
                });
          },
          icon: Icon(Icons.playlist_add_check, color: Colors.white,),
          label: Text("Apply settings", style: TextStyle(color: Colors.white),),
          backgroundColor: Colors.red,
        ),
        appBar: AppBar(
          title: Container(
            child: Text(
              'Settings',
              style: TextStyle(color: Colors.white),
            ),
          ),

          leading: BackButton(color: Colors.white, onPressed: (){
              print("Closing");
              Navigator.of(context).pop();
            },
          ),

          backgroundColor: Colors.transparent,
          elevation: 0.0,
          actions: <Widget>[
          ],
        ),
        body: Container(
          child: Container(
              padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                child: new ListView(
                    children: <Widget>[
                      new Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),),

                      SizedBox(height: 10,),

                      // Название блока CONTACT
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "CONTACT", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                        ),
                        ),
                      ),

                      SizedBox(height: 15,),

                      // Название блок "Name"
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "First name", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 14,
                        ),
                        ),
                      ),

                      // Блок ввода данных Name
                      Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: Colors.white,
                        ),
                        child: new Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child:    TextFormField(
                            autofocus: false,

                            style: TextStyle(color: Colors.white),
                            maxLines: null, // ne
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              //errorText: "",
                              errorStyle: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,//this has no effect
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  )
                              ),

                              focusColor: Colors.white,

                              fillColor: Colors.white,
                              hoverColor: Colors.white,
                              border: OutlineInputBorder(),
                              // Вывод данных индекса пользователя в блок до нажатия
                              hintText: _name,

                              //counterText: _index,
                              //suffixText: _index,
                              //helperText: _name,
                              //semanticCounterText: _name,
                              labelStyle:  TextStyle(color: Colors.white,),
                            ),
                            onChanged: (value) async {
                              _name = value;
                            },
                            initialValue: _name,
                          ),
                        ),
                      ),

                      SizedBox(height: 30,),

                      // Название блок "Surname"
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "Second name", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 14,
                        ),
                        ),
                      ),

                      // Блок ввода данных Surname
                      Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: Colors.white,
                        ),
                        child: new Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child:    TextFormField(
                            autofocus: false,

                            style: TextStyle(color: Colors.white),
                            maxLines: null, // ne
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              //errorText: "",
                              errorStyle: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,//this has no effect
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  )
                              ),

                              focusColor: Colors.white,

                              fillColor: Colors.white,
                              hoverColor: Colors.white,
                              border: OutlineInputBorder(),
                              //labelText: 'Edit',

                              // Вывод данных индекса пользователя в блок до нажатия
                              hintText: _surname,

                              //counterText: _index,
                              //suffixText: _index,
                              //helperText: _surname,
                              //semanticCounterText: _surname,
                              labelStyle:  TextStyle(color: Colors.white,),
                            ),
                            onChanged: (value) async {
                              _surname = value;
                            },
                            initialValue: _surname,
                          ),
                        ),
                      ),

                      SizedBox(height: 30,),

                      // Название блок "Email"
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "Email", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 14,
                        ),
                        ),
                      ),

                      // Блок ввода данных Email
                      Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: Colors.white,
                        ),
                        child: new Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child:    TextFormField(
                            autofocus: false,

                            style: TextStyle(color: Colors.white),
                            maxLines: null, // ne
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              //errorText: "",
                              errorStyle: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,//this has no effect
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,
                                )
                              ),

                              focusColor: Colors.white,

                              fillColor: Colors.white,
                              hoverColor: Colors.white,
                              border: OutlineInputBorder(),

                              //labelText: 'Edit',

                              // Вывод данных индекса пользователя в блок до нажатия
                              hintText: _email,

                              //counterText: _index,
                              //suffixText: _index,
                              //helperText: "Email",

                              //semanticCounterText: _email,
                              labelStyle:  TextStyle(color: Colors.white,),
                            ),
                            onChanged: (value) async {
                                  _email = value;
                            },
                            initialValue: _email,
                          ),
                        ),
                      ),

                      SizedBox(height: 30,),

                      // Разделитель блоков
                      new Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0),),

                      SizedBox(height: 10,),

                      // Название блока INFORMATION
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "INFORMATION", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                        ),
                        ),
                      ),

                      SizedBox(height: 15,),

                      // Название блок "Country"
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "Country", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 14,
                        ),
                        ),
                      ),

                      // Блок ввода данных Country
                      Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: Colors.white,
                        ),
                        child: new Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child:    TextFormField(
                            autofocus: false,

                            style: TextStyle(color: Colors.white),
                            maxLines: null, // ne
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              //errorText: "",
                              errorStyle: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,//this has no effect
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  )
                              ),

                              focusColor: Colors.white,

                              fillColor: Colors.white,
                              hoverColor: Colors.white,
                              border: OutlineInputBorder(),

                              //labelText: 'Edit',

                              // Вывод данных индекса пользователя в блок до нажатия
                              hintText: _country,

                              //counterText: _index,
                              //suffixText: _index,
                              //helperText: _country,
                              //semanticCounterText: _country,
                              labelStyle:  TextStyle(color: Colors.white,),
                            ),
                            onChanged: (value) async {
                              _country = value;
                            },
                            initialValue: _country,
                          ),
                        ),
                      ),

                      SizedBox(height: 30,),

                      // Название блок "Region"
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "Region", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 14,
                        ),
                        ),
                      ),

                      // Блок ввода данных Region
                      Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: Colors.white,
                        ),
                        child: new Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child:    TextFormField(
                            autofocus: false,

                            style: TextStyle(color: Colors.white),
                            maxLines: null, // ne
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              //errorText: "",
                              errorStyle: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,//this has no effect
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  )
                              ),

                              focusColor: Colors.white,

                              fillColor: Colors.white,
                              hoverColor: Colors.white,
                              border: OutlineInputBorder(),
                              //labelText: 'Edit',

                              // Вывод данных индекса пользователя в блок до нажатия
                              hintText: _region,

                              //counterText: _index,
                              //suffixText: _index,
                              helperText: _region,
                              semanticCounterText: _region,
                              labelStyle:  TextStyle(color: Colors.white,),
                            ),
                            onChanged: (value) async {
                              _region = value;
                            },
                            initialValue: _region,
                          ),
                        ),
                      ),

                      SizedBox(height: 30,),

                      // Название блок "ZIP / Postal Code"
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                        child: Text(
                          "ZIP / Postal Code", textAlign: TextAlign.left, style: TextStyle(
                          color: Colors.white, fontSize: 14,
                        ),
                        ),
                      ),

                      // Блок ввода данных ZIP / Postal Code
                      Theme(
                        data: Theme.of(context).copyWith(
                          unselectedWidgetColor: Colors.white,
                        ),
                        child: new Container(
                          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child:    TextFormField(
                            autofocus: false,

                            style: TextStyle(color: Colors.white),
                            maxLines: null, // ne
                            keyboardType: TextInputType.text,
                            decoration: InputDecoration(
                              //errorText: "",
                              errorStyle: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                              ),
                              disabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                  color: Colors.white,//this has no effect
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    color: Colors.white,
                                  )
                              ),

                              focusColor: Colors.white,

                              fillColor: Colors.white,
                              hoverColor: Colors.white,
                              border: OutlineInputBorder(),

                              //labelText: 'Edit',

                              // Вывод данных индекса пользователя в блок до нажатия
                              hintText: _index,

                              //counterText: _index,
                              //suffixText: _index,
                              //helperText: _index,
                              //semanticCounterText: _index,
                              labelStyle:  TextStyle(color: Colors.white,),
                            ),
                            onChanged: (value) async {
                              _index = value;
                            },
                            initialValue: _index,
                          ),
                        ),
                      ),
                      SizedBox(height: 100,),
                    ]
                )
            ),
          )

    );
  }
}

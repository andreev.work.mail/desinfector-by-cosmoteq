
import 'package:virusaround/DataBaseAccess/FBManager.dart';
import 'package:virusaround/Settings/UserSettings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'LogsView.dart';

class OrderChatView extends StatefulWidget {
  final orderToken;
  var orderDocument;
  OrderChatView({@required this.orderToken, this.orderDocument});

  @override
  _OrderChatView createState() => _OrderChatView();
}

class _OrderChatView extends State<OrderChatView>{
  final orderToken;
  String _messageText;
  Stream messageStream;
  TextEditingController _textEditingController;
  _OrderChatView({@required this.orderToken});


  void sendMessage(String text, String orderToken) {
    if (_messageText != null || _messageText.trim() != ' ')
      Firestore.instance
          .collection('chatrooms')
          .document('123')//${widget.orderToken+UserSettings.UID+widget.orderDocument['token']}
          .collection('messages')
          .add({
        "user" : UserSettings.UID,
        "text" : text,
        "post_time" : Timestamp.now(),
        "name" : "Какашка",//UserSettings.userDocument['name']
      });
    _textEditingController.clear();
    _messageText = '  ';

  }

  @override
  void initState(){
    messageStream =
        Firestore.instance
            .collection('chatrooms')
            .document('123')
            .collection('messages')
            .orderBy("post_time")
            .snapshots();
    print(messageStream);
    _textEditingController = new TextEditingController(text: _messageText);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Company name'),
        backgroundColor: Color.fromARGB(100, 100, 100, 100),
      ),
      body: Container(
        child: Column(
            children: <Widget>[
              Flexible(
                child: StreamBuilder<QuerySnapshot>(
                  stream: messageStream,
                  builder:
                      (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    print("1");
                    print(snapshot.toString());
                    if (snapshot.data == null) {
                      return Center(child: Text("Пусто"),);
                    }
                    print("2");
                    if (snapshot.hasError)
                      return new Text('Error: ${snapshot.error}');
                    switch (snapshot.connectionState) {
                      case ConnectionState.waiting:

                        return Text("Загрузка");
                      default:
                        print("3");
//                        if (snapshot.data.documents.isEmpty)
//                          return Text("ERR");
                        return ListView(
                          children: snapshot.data.documents
                              .map((DocumentSnapshot document) {
                            print("4");
                            return new ChatMessage(
                                document: document);
                          }).toList(),
                        );
                    }
                  },
                ),
              ),
              Divider(height: 1.0),
              Container(
                decoration: BoxDecoration(
                    color: Theme.of(context).cardColor),
                child: IconTheme(
                  data: IconThemeData(color: Theme.of(context).accentColor),
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Row(children: <Widget>[
                      Flexible(
                        child:  TextField(
                          controller: _textEditingController,
                          onChanged: (String text) {
                            _messageText = text;
                          },
                          decoration:
                          InputDecoration.collapsed(hintText: "Сообщение"),
                        ),
                      ),
                      Container(
                          margin: EdgeInsets.symmetric(horizontal: 4.0),
                          child: IconButton(
                            icon: Icon(Icons.send, color: Colors.red,),
                            onPressed: (){sendMessage(_messageText, widget.orderToken);},
                          )
                      ),
                    ]),
                  ),
                ),
              )
            ]
        ), // Sticker
      ),
    );
  }
}

class ChatMessage extends StatelessWidget {
  ChatMessage({this.document});
  final DocumentSnapshot document;
  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(right: 16.0),
                child: CircleAvatar(child: Text(document['name'][0]), backgroundColor: (document['user'] as String) == UserSettings.UID ? Colors.green : Colors.white, foregroundColor: (document['user'] as String) == UserSettings.UID ? Colors.white : Colors.green),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(document['name'], style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 10.0,
                        color: Colors.black26)),
                    Text((document['post_time'] as Timestamp).toDate().toIso8601String(), style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 8.0,
                        color: Colors.black12)),
                    Container(
                      margin: const EdgeInsets.only(top: 5.0),
                      child: Text(document['text']),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}
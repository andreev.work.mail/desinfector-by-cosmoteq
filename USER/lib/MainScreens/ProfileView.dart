import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:virusaround/DataBaseAccess/FBManager.dart';
import 'package:virusaround/DataBaseAccess/NotifManager.dart';
import 'package:virusaround/Entry/UsersOnboarding.dart';
import 'package:virusaround/Settings/userSettings.dart';

import 'ChatRoom.dart';
import 'DeactivatedOrdersView.dart';
import 'LogsView.dart';
import 'PaymentView.dart';
import 'ProfileSettingsView.dart';

void editProfile()
{
  print("KAKA! KAKASHKA!");
}

bool _readyToBuild = false;

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

bool changedBalance = false;

class _DashboardPageState extends State<DashboardPage> {
  Timer _timer;
  int _start = 5;

  void startTimer() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(oneSec, (Timer timer) {
      if (_start < 1) timer.cancel();
      if (UserSettings.userDocument != null)
        setState(() {
          timer.cancel();
          _readyToBuild = true;
        });
      --_start;
    });
  }

  @override
  void initState() {

    FirebaseAuth.instance.currentUser().then((val) {
      setState(() {
        UserSettings.UID = val.uid;
      });
    }).catchError((e) {
      Logs.addNode("AuthScreen", "initState", e.toString());
    });
    super.initState();
  }

  String setName() {
    if (UserSettings.userDocument == null) return "Loading";
    return UserSettings.userDocument['name'];
  }

  String setSurname() {
    if (UserSettings.userDocument == null) return "Loading";
    return UserSettings.userDocument['surname'];
  }

  String setBalance(var balance) {
    if (balance != null)
      return
        UserSettings.userDocument['balance'].toString() +
            " \$";
    else
      return "Loading";
  }

  addDeviceTokemToLogs() async{
    Logs.addNode("Auth Screen", "deviceToken", await NotifManager.GetDeviceToken());
  }

  @override
  Widget build(BuildContext context) {
    Logs.addNode("NotifManager", "onMessage", "Called");
    addDeviceTokemToLogs();
    if (UserSettings.userDocument == null)
      startTimer();
    else
      _readyToBuild = true;
    if (!_readyToBuild) {
      return new Scaffold(
        body: Center(
          child: Text("Loading"),
        ),
      );
    }
    if (changedBalance) var updateBalance = FBManager.updateBalance();
    return new Scaffold(
      body: Container(
        child: Container(
          padding: const EdgeInsets.fromLTRB(10, 70, 10, 0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              // Блок header
              Row(
                  children: <Widget>[

                    // Блок имени пользователя
                    Container(
                        padding: const EdgeInsets.fromLTRB(14, 0, 0, 10),
                        child: CircleAvatar(
                          radius: 40,
                          backgroundColor: Colors.red,
                          child: Text(setName()[0] + setSurname()[0], style: (TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0)),) ,
                          foregroundColor: Colors.white,
                        )
                    ),

                    // Блок имени и фамилии пользователя
                    Expanded(
                      flex: 2, // 60%
                      child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                  Container(
                                  padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
                                  child: Text("  " + setName() + "\n  " + setSurname() , style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0, ),textAlign: TextAlign.left,),
                                ),
                              ],
                            ),
                          ]
                      ),
                    ),

                    // Кнопка редактирвоания данных профиля
                    Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 50),
                      child: Ink(
                        width: 35.0,
                        height: 35.0,
                        decoration: const ShapeDecoration(
                          color: Color.fromARGB(100, 100, 100, 100),
                          shape: CircleBorder(),

                        ),
                        child: IconButton(
                          icon: Icon(Icons.edit),
                          color: Colors.white,
                          iconSize: 20,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileSettings()));
                          },
                        ),
                      ),
                    ),
                  ]
              ),

              // Разделяющий блокк
              Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
                margin: const EdgeInsets.only(left: 10.0, right: 10.0),),

              SizedBox(height: 10,),

              // Блок основной информации о пользователе
              Container(
                  padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                  child: new Column(
                      children: <Widget>[

                        // Первый блок с контактной информацией
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                              child: Text(
                                "CONTACT", textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                              ),
                              ),
                            ),
                          ],
                        ),

                        // Название блока Phone
                        Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                                child: Text(
                                  "Phone", textAlign: TextAlign.left, style: TextStyle(
                                  color: Colors.white, fontSize: 14,
                                ),
                                ),
                              ),
                            ]
                        ),

                        // Блок вывода номер телефона phone
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 5),
                              child: Text(
                                UserSettings.userDocument['phone'].toString(), textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                              ),
                              ),
                            ),
                          ],
                        ),

                        // Блок названия Email
                        Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                                child: Text(
                                  "Email", textAlign: TextAlign.left, style: TextStyle(
                                  color: Colors.white, fontSize: 14,
                                ),
                                ),
                              ),
                            ]
                        ),

                        // Блок вывода данных email
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 5),
                              child: Text(
                                UserSettings.userDocument['email'].toString() ?? "Not stated", textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                              ),
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: 10,),

                        new Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
                          margin: const EdgeInsets.only(left: 10.0, right: 10.0),),

                        SizedBox(height: 10,),

                        // Блок адресной информации о пользователе
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                              child: Text(
                                "INFORMATION", textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                              ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                              child: Text(
                                "Country", textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 14,
                              ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  UserSettings.userDocument['country'].toString() ?? "Not stated", textAlign: TextAlign.left, style: TextStyle(
                                  color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                                ),
                                ),
                              ),
                            ]
                        ),
                        Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                                child: Text(
                                  "Region", textAlign: TextAlign.left, style: TextStyle(
                                  color: Colors.white, fontSize: 14,
                                ),
                                ),
                              ),
                            ]
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 0, 0, 5),
                              child: Text(
                                UserSettings.userDocument['region'].toString(), textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                              ),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                              child: Text(
                                "ZIP / Postal Code", textAlign: TextAlign.left, style: TextStyle(
                                color: Colors.white, fontSize: 14,
                              ),
                              ),
                            ),
                          ],
                        ),


                        Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.fromLTRB(10, 0, 0, 5),
                                child: Text(
                                  UserSettings.userDocument['index'].toString() ?? "Not stated", textAlign: TextAlign.left, style: TextStyle(
                                  color: Colors.white, fontSize: 12, fontWeight: FontWeight.w300,
                                ),
                                ),
                              ),
                            ]
                        ),
                      ]
                  )
              ),

              SizedBox(height: 10,),

              // Блок разделителя
              Container(padding: const EdgeInsets.fromLTRB(0, 10, 0, 0), height: 1, color: Colors.black12,
                margin: const EdgeInsets.only(left: 10.0, right: 10.0),),

              // Блок выхода из аккаунта
              Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                  child: FlatButton(
                    child: Text('Log out ' ?? 'Error name', style: TextStyle(fontWeight: FontWeight.w500, fontSize: 14.0, color: Colors.red),textAlign: TextAlign.start,),
                    onPressed: () {
                      exitUser();
                    },
                  )
              ),
            ],
          ),
        ),
      ),

    );
  }


  Future<void> exitUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
    UserSettings.clearAll();
    FirebaseAuth.instance.signOut().then((action) {
      while (Navigator.canPop(context)) Navigator.canPop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => OnBoarding()));
    }).catchError((e) {
      print("AuthScreen: " + e);
    });
  }

  getTok() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    String token = (await user.getIdToken()).token;
    String url =
        "https://us-central1-cleaner-1d1ca.cloudfunctions.net/app/robokassa/payment-form?token=${token}";
    print(url);
    return url.toString();
  }
}
